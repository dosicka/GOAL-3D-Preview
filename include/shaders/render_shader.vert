#version 430
// this code is executed for each vertex in scene

// vertex must be vec4 because of matrix transformation (homogenous coordinates)
// 4. value is always 1.0 for point and 0.0 for vector
layout(location = 0) in vec4 vertex;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv_coord;

uniform mat4 model_matrix;
// camera matrices
uniform mat4 camera_projection_matrix;
uniform mat4 camera_view_matrix;
uniform mat3 normal_matrix;
// light matrices
uniform mat4 light_projection_matrix;
uniform mat4 light_view_matrix;

out vec4 vertex_camSpace;
out vec3 normal_camSpace;
out vec2 texture_coord;
out vec4 vertex_lightSpace;


void main(void)
{
    vertex_camSpace = camera_view_matrix * model_matrix * vertex;
    normal_camSpace = normalize(normal_matrix * normal);
    texture_coord = uv_coord;
    vertex_lightSpace = light_projection_matrix * light_view_matrix * model_matrix * vertex;

    gl_Position = camera_projection_matrix * camera_view_matrix * model_matrix * vertex;
}
