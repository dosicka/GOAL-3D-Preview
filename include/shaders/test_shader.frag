#version 430 core

in vec2 UV;

uniform sampler2D shadow_map;

out vec4 FragColor;


float LinearizeDepth(float depth, float near_plane, float far_plane)
{
    float z = depth * 2.0 - 1.0; // Back to NDC
    return (2.0 * near_plane * far_plane) / (far_plane + near_plane - z * (far_plane - near_plane));
}

void main()
{
    float near_plane = 1;
    float far_plane = 1000;

    // perspective
//    float depthValue = texture(shadow_map, UV).r;
//    FragColor = vec4(vec3(LinearizeDepth(depthValue, near_plane, far_plane) / far_plane), 1.0);

    // orthographic
    FragColor = texture(shadow_map, UV) /12;
}
