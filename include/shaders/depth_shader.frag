#version 430 core

in vec4 vertex_modelSpace;

// make uniform if neccessary to use
 float near_clip = 1;
 float far_clip = 19;

// output for hight map for position selector
layout(location = 0) out float output_val;
// for shadow map is no output, set automatically from gl_FragDepth


// return real depth not clamped to range [0, 1]
float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // First transform the depth value back to NDC range [-1, 1]
    return (2.0 * near_clip * far_clip) / (far_clip + near_clip - z * (far_clip - near_clip)); // then apply the inverse transformation to retrieve the linear depth value
}

void main()
{
    // use logarithmic function - default one
//    output_val = gl_FragCoord.z;
    // use linear function
//    output_val = LinearizeDepth(gl_FragCoord.z) / far_clip; // divide by far for demonstration
    // use real height
    output_val = vertex_modelSpace.y;
}
