#version 430 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec4 vertex;

// Values that stay constant for the whole mesh.
uniform mat4 light_projection_matrix;
uniform mat4 light_view_matrix;
uniform mat4 model_matrix;

out vec4 vertex_modelSpace;

void main(){
    vertex_modelSpace = model_matrix * vertex;
    gl_Position = light_projection_matrix * light_view_matrix * model_matrix * vertex;
}
