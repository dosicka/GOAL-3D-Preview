#version 430
// this code is executed for each pixel in scene

// IN FROM VERTEX SHADER
// vertex, normal, uv
in vec4 vertex_camSpace; // camera space
in vec3 normal_camSpace; // camera space
in vec2 texture_coord;
in vec4 vertex_lightSpace; // light space

// STRUCTS
// material
struct MaterialStruct {
    vec4 ambient_coef;
    vec4 diffuse_coef;
    vec4 specular_coef;
    float shines_coef;
    float alpha;
};
// light
struct LightStruct {
    vec4 position;
    vec4 direction;
    vec4 color;
    float intensity;
    float cutoff; // Cutoff angle (between 0 and 90) - velikost osvětleného kruhu
    float angle_attenuation_coef; // Angular attenuation exponent - rychlost úbytku světla od středu
    float dist_attenuation_coef; // can go above 1.0 (when more lite, must increase, not decrease)
};

// LAYOUTS AND UNIFORMS FROM C++
//uniform MaterialStruct material;
layout(std140, binding=1) uniform material_buffer
{ // dynamickou délku může mít jen jedno pole v bufferu
    MaterialStruct material;
};
layout(std430, binding=2) buffer lights_buffer_1
{
    LightStruct main_lights[];
};
layout(std430, binding=3) buffer lights_buffer_2
{ // dynamickou délku může mít jen jedno pole v bufferu
    LightStruct screen_lights[];
};
// matrices
uniform mat4 camera_view_matrix;
uniform mat4 model_matrix;
uniform mat3 normal_matrix;
// textures
uniform sampler2D object_texture;
uniform sampler2D screen_texture;
uniform sampler2D shadow_map_opaque;
uniform sampler2D shadow_map_glass;
// other
uniform int render_style;
uniform float screen_brightness;

//// there is no build in variable for fragment color as used to be gl_FragColor till version 120
//// now I must specify out variable for this
out vec4 frag_color;

// GLOBALY DEFINED VARIABLES
// fixed in position (0,0,0) or (0,0,1) - origin of camera space and everything else is camera space
vec3 camera_position = vec3(0, 0, 1);


vec4 diffuse_specular_point_lighting(LightStruct _light)    // POINT LIGHT - from one point to all directions (fire)
{
    // translate light position and direction to camera space - přesunout do C++
    vec3 light_direction = normalize(normal_matrix * _light.direction.xyz);
    vec3 light_position = vec3(camera_view_matrix * model_matrix * _light.position);

    // shading back sides of faces (GL_CULL_FACE must be disabled)
    vec3 normal_camSp = normalize(normal_camSpace); // tnorm
    if(!gl_FrontFacing){
        normal_camSp = normalize(-normal_camSpace);
    }
    // vector from vertex to light source
    vec3 to_light_ray = normalize(light_position - vertex_camSpace.xyz);  //s
    // vector from vertex to camera
    vec3 to_camera_ray = normalize(camera_position - vertex_camSpace.xyz);   //v
    // reflected ray of light = reflection of light ray coming to vertex
    vec3 reflected_ray = reflect(-to_light_ray, normal_camSp); //r
    // cos of angle btw the vector from vertex to light source and normal
    float cos_theta = clamp(dot(to_light_ray, normal_camSp), 0, 1);   // sDotN
    // distance of light from vertex
    float light_distance = length(light_position - vertex_camSpace.xyz);
    // ATTENUATION - model how much light is available for this fragment
    float dist_attenuation = 1.0 / (1.0 + _light.dist_attenuation_coef * pow(light_distance, 2)); // kvadratická
//    float dist_attenuation = 1.0 / (1.0 + _light.dist_attenuation_coef * light_distance); // linearni

    vec4 diffuse = vec4(0);
    vec4 specular = vec4(0);

    // diffuse - depends on angle btw normal and vector from vertex to light source
    diffuse = material.diffuse_coef * _light.color * _light.intensity * cos_theta * dist_attenuation;
    // specular - depends on angle btw reflected ray and vector from vertex to camera(eye)
    if(cos_theta > 0.0) {   // if angle btw normal and vector from vertex to light = 0 -> no specular light
        specular = material.specular_coef * _light.color * _light.intensity * dist_attenuation *
                pow(max(dot(reflected_ray, to_camera_ray), 0.0), material.shines_coef);
    }

    vec4 rgba = clamp((diffuse + specular), vec4(0), vec4(1));
    return rgba;
}

vec4 diffuse_specular_spot_lighting(LightStruct _light) // SPOT LIGHT - from one point a cone of light (torch)
{
    // translate light position and direction to camera space - přesunout do C++
    vec3 light_direction = normalize(normal_matrix * _light.direction.xyz);
    vec3 light_position = vec3(camera_view_matrix * model_matrix * _light.position);

    // shading back sides of faces (GL_CULL_FACE must be disabled)
    vec3 normal_camSp = normalize(normal_camSpace); // tnorm
    if(!gl_FrontFacing){
        normal_camSp = normalize(-normal_camSpace);
    }
    // vector from vertex to light source
    vec3 to_light_ray = normalize(light_position - vertex_camSpace.xyz);  //s
    // vector from vertex to camera
    vec3 to_camera_ray = normalize(camera_position - vertex_camSpace.xyz);   //v
    // reflected ray of light = reflection of light ray coming to vertex
    vec3 reflected_ray = reflect(-to_light_ray, normal_camSp); //r
    // cos of angle btw the vector from vertex to light source and normal
    float cos_theta = clamp(dot(to_light_ray, normal_camSp), 0, 1);   // sDotN
    // cos of angle btw light direction and vector from light source to vertex
    float cos_theta_light = clamp(dot(normalize(light_direction), -to_light_ray), 0, 1); // nevim esi je potřeba dělat clamp
    float spotFactor = pow(cos_theta_light, _light.angle_attenuation_coef);
    // angle in radians between light direction and vector from vertex to light
    float angle = acos(dot(-to_light_ray, light_direction));
    // angle in radians to cut off the light
    float cutoff = radians(clamp(_light.cutoff, 0.0, 90.0));
    // distance of light from vertex
    float light_distance = length(light_position - vertex_camSpace.xyz);
    // ATTENUATION - model how much light is available for this fragment
    float dist_attenuation = 1.0 / (1.0 + _light.dist_attenuation_coef * pow(light_distance, 2)); // kvadratická
//    float dist_attenuation = 1.0 / (1.0 + light.dist_attenuation_coef * light_distance); // linearni
    vec3 h = normalize(to_camera_ray + to_light_ray);

    vec4 diffuse = vec4(0);
    vec4 specular = vec4(0);

    // if angle is greater then the cutoff angle -> no lighting
    if(angle < cutoff) {
        // diffuse - depends on angle btw normal and vector from vertex to light source
        // and angle btw light direction and vector from light source to vertex
        // (the bigger angle btw the vector and  light direction, the lower light intensity)
        diffuse = material.diffuse_coef * _light.color * _light.intensity * spotFactor * dist_attenuation * cos_theta;
        // specular - depends on angle btw reflected ray and vector from vertex to camera(eye)
        // and angle btw light direction and vector from light source to vertex
        specular = material.specular_coef * _light.color * _light.intensity * spotFactor * dist_attenuation *
//                pow(max(dot(h, normal_camSp), 0.0), material.shines_coef); // asi stejné jako řádek níže
                pow(max(dot(reflected_ray, to_camera_ray), 0.0), material.shines_coef);
    }

    vec4 rgba = clamp((diffuse + specular), vec4(0), vec4(1));
    return rgba;
}

vec4 diffuse_specular_directional_lighting(LightStruct _light)  //DIRECTIONAL LIGHT - one direction of light in whole scene (like from many points) (sun)
{
    // translate light direction to camera space - přesunout do C++
    vec3 light_direction = normalize(normal_matrix * _light.direction.xyz);

    // shading back sides of faces (GL_CULL_FACE must be disabled)
    vec3 normal_camSp = normalize(normal_camSpace); // tnorm
    if(!gl_FrontFacing){
        normal_camSp = normalize(-normal_camSpace);
    }
    // vector from vertex to camera
    vec3 to_camera_ray = normalize(camera_position - vertex_camSpace.xyz);   //v
    // reflected ray of light = reflection of light ray coming to vertex
    vec3 reflected_ray = reflect(light_direction, normal_camSp); //r
    // cos of angle btw the vector from vertex to light source and normal
    float cos_theta = clamp(dot(-light_direction, normal_camSp), 0, 1);   // sDotN

    vec4 diffuse = vec4(0);
    vec4 specular = vec4(0);

    // diffuse - depends on angle btw normal and oposite of light direction
    //  (directional light is same diretion everywhere -> don't care about vector from vertex to light)
    diffuse = material.diffuse_coef * _light.color * _light.intensity * cos_theta;
    // specular - depends on angle btw reflected ray and vector from vertex to camera(eye)
    if(cos_theta > 0.0) {   // if angle btw normal and vector from vertex to light = 0 -> no specular light
      specular = material.specular_coef * _light.color * _light.intensity *
              pow(max(dot(reflected_ray, to_camera_ray), 0.0), material.shines_coef);
    }

    vec4 rgba = clamp((diffuse + specular), vec4(0), vec4(1));
    return rgba;
}

vec4 diffuse_spot_lighting(LightStruct _light)  //SPOT LIGHT - from one point a cone of light (torch)
{
    // translate light position and direction to camera space - přesunout do C++
    vec3 light_direction = normalize(normal_matrix * _light.direction.xyz);
    vec3 light_position = vec3(camera_view_matrix * model_matrix * _light.position);

    // shading back sides of faces (GL_CULL_FACE must be disabled)
    vec3 normal_camSp = normalize(normal_camSpace);
    if(!gl_FrontFacing){
        normal_camSp = normalize(-normal_camSpace);
    }
    // vector from vertex to light source
    vec3 to_light_ray = normalize(light_position - vertex_camSpace.xyz);
    // cos of angle btw the vector from vertex to light source and normal
    float cos_theta = clamp(dot(to_light_ray, normal_camSp), 0, 1);
    // cos of angle btw light direction and vector from light source to vertex
    float cos_theta_light = clamp(dot(normalize(light_direction), -to_light_ray), 0, 1); // nevim esi je potřeba dělat clamp
    // spotFactor gives strength of light for current vertex according to angle attenuation
    float spotFactor = pow(cos_theta_light, _light.angle_attenuation_coef);
    // angle in radians between light direction and vector from vertex to light
    float angle = acos(dot(-to_light_ray, light_direction));
    // angle in radians to cut off the light
    float cutoff = radians(clamp(_light.cutoff, 0.0, 90.0));
    // distance of light from vertex
    float light_distance = length(light_position - vertex_camSpace.xyz);
    // ATTENUATION - model how much light is available for this fragment
    float dist_attenuation = 1.0 / (1.0 + _light.dist_attenuation_coef * pow(light_distance, 2)); // kvadratická - fyzikálně správně
//    float dist_attenuation = 1.0 / (1.0 + _light.dist_attenuation_coef * light_distance ); // linearni - vhodné pro PC grafiku

    vec4 diffuse = vec4(0);

    // if angle is greater then the cutoff angle -> no lighting
    if(angle < cutoff) {
        // diffuse - depends on angle btw normal and vector from vertex to light source
        // and angle btw light direction and vector from light source to vertex
        // (the bigger angle btw the vector and  light direction, the lower light intensity)
        diffuse = material.diffuse_coef * _light.color * _light.intensity * spotFactor * dist_attenuation * cos_theta;
    }

    vec4 rgba = clamp((diffuse), vec4(0), vec4(1));
    return rgba;
}


float ShadowCalculation(vec4 vertex_lightSpace, sampler2D shadow_map, LightStruct _light)
{
    vec3 light_direction = normalize(normal_matrix * _light.direction.xyz);
    vec3 normal_camSp = normalize(normal_camSpace);
    if(!gl_FrontFacing){
        normal_camSp = normalize(-normal_camSpace);
    }

    // perform perspective divide - for perspective projection, but could be in orthographic too
    vec3 projCoords = vertex_lightSpace.xyz / vertex_lightSpace.w;
    // transform to [0,1] range
    projCoords = projCoords * 0.5 + 0.5;
    // get depth of current fragment from light's perspective
    float currentDepth = projCoords.z;

    // bias to remove shadow acne in scene - stripes of shadow
    float bias = max(0.00005 * (1.0 - dot(normal_camSp, light_direction)), 0.000005);
    // get closest depth value from light's perspective (using [0,1] range fragPosLight as coords)
    float closestDepth = texture(shadow_map, projCoords.xy).r;   // only R component contains depth value
    // check whether current frag pos is in shadow
    float shadow = currentDepth - bias > closestDepth ? 0.5 : 0.0; // no PCF -> jagged shadow

//    float bias = max(0.00005 * (1.0 - dot(normal_camSp, light_direction)), 0.000005);
//    // reduce jagging by sampling neighbour texels from texture. The more samples used, the smoother shadows
//    float shadow = 0.0;
//    vec2 texelSize = 1.0 / textureSize(shadow_map, 0);
//    for(int x = -5; x <= 5; ++x) // number of samples in x axis
//    {
//        for(int y = -5; y <= 5; ++y) // number of samples in y axis
//        {
//            float pcfDepth = texture(shadow_map, projCoords.xy + vec2(x, y) * texelSize).r;
//            shadow += currentDepth - bias > pcfDepth  ? 0.5 : 0.0;
//        }
//    }
//    shadow /= 121.0; // divided by number of samples (x * y)

//        // if shadow_map is shadow sampler
//        shadow = texture(shadow_map, vec3(vertex_lightSpace.xy, (vertex_lightSpace.z-bias)/vertex_lightSpace.w));

    // when no shadow behind far clip plane, else there will be shadow
    if(projCoords.z > 1.0){
        shadow = 0.0;
    }

    return shadow;
}


void main(void)
{
    // SCREENS
    if(render_style == 1)
    {
        //////////////////// obrazovky by asi neměly byt uplně černý, když světla svítí naplno
        frag_color = vec4(texture(screen_texture, texture_coord).rgb * screen_brightness, 1.0);
    }
    // LIGHTS OBJECT
    else if(render_style == 2)
    {
        frag_color = vec4(main_lights[0].color.rgb * main_lights[0].intensity, 1.0);
    }

    // OBJECTS
    if(render_style == 3)
    {
        vec3 diff_spec = diffuse_specular_directional_lighting(main_lights[0]).rgb + diffuse_specular_point_lighting(main_lights[1]).rgb;
        // tmavost stínu je menší, čím větší intenzita světla - není správně
//        float shadow = light.intensity - ShadowCalculation(vertex_lightSpace);
        // tmavost stínu je konstantní, bez ohledu na intenzitu světla, jen ambient složka se zvětšuje
        float shadow1 = ShadowCalculation(vertex_lightSpace, shadow_map_opaque, main_lights[0]);
        float shadow2 = ShadowCalculation(vertex_lightSpace, shadow_map_glass, main_lights[0]);
        float shadow_intersection = max((shadow1 + shadow2) - 0.5, 0);
        float shadow = 1.0 - (shadow1 + shadow2*0.5 - shadow_intersection*0.5);
        // ambientní složky by měla být trochu světlejší s vyšší intenzitou světla, proto násobeno intenzitou
        vec3 ambient = (material.ambient_coef * main_lights[0].color * main_lights[0].intensity).rgb;
        vec3 color1 = ambient + shadow * diff_spec;

        vec3 diffuse = vec3(0);
        for(int i=0; i<screen_lights.length(); i++){
            diffuse += diffuse_spot_lighting(screen_lights[i]).rgb;
        }
//        vec3 light_intensity = screen_lights[0].color.rgb * screen_lights[0].intensity;
//        vec3 ambient = material.ambient_coef.rgb * light_intensity * 0.1;
        vec3 color2 = diffuse /* + ambient*/;

        frag_color = vec4(color1*color1 + color2*color2, material.alpha);
    }
    // ICE
    else if(render_style == 4)
    {
        vec3 diff_spec = diffuse_specular_directional_lighting(main_lights[0]).rgb + diffuse_specular_point_lighting(main_lights[1]).rgb;
        // tmavost stínu je menší, čím větší intenzita světla - není správně
//        float shadow = light.intensity - ShadowCalculation(vertex_lightSpace);
        // tmavost stínu je konstatní, bez ohledu na intenzitu světla, jen ambient složka se zvětšuje
//        float shadow = 1.0 - (ShadowCalculation(vertex_lightSpace, shadow_map_opaque, 0.5f) + ShadowCalculation(vertex_lightSpace, shadow_map_glass, 0.3f));
        // ambientní složky by měla být trochu světlejší s vyšší intenzitou světla, proto násobeno intenzitou
        vec3 ambient = (material.ambient_coef * main_lights[0].color * main_lights[0].intensity).rgb;
        vec3 textura = texture(object_texture, texture_coord).rgb;
        vec3 video_mapping = texture(screen_texture, texture_coord).rgb * screen_brightness;
        vec3 color1 = (ambient + /*shadow **/ diff_spec) * textura + video_mapping;

        vec3 diffuse = vec3(0);
        for(int i=0; i<screen_lights.length(); i++){
            diffuse += diffuse_spot_lighting(screen_lights[i]).rgb;
        }
//        vec3 light_intensity = screen_lights[0].color.rgb * screen_lights[0].intensity;
//        vec3 ambient = material.ambient_coef.rgb * light_intensity * 0.5;
        vec3 color2 = diffuse; // (diffuse + ambient) * (textura + video_mapping)

        frag_color = vec4(color1*color1 + color2*color2, material.alpha);
    }


//    // OBJECTS - LIGHT BUT NO SHADOWS (SCREEN LIGHTS)
//    else if(render_style == 4)
//    {
//        vec3 diffuse = vec3(0);
//        for(int i=0; i<screen_lights.length(); i++){
//            diffuse += diffuse_spot_lighting(screen_lights[i]).rgb;
//        }
////        vec3 light_intensity = screen_lights[0].color.rgb * screen_lights[0].intensity;
////        vec3 ambient = material.ambient_coef.rgb * light_intensity * 0.1;
//        frag_color = vec4(diffuse/* + ambient*/, material.alpha);
//    }

//    // ICE - LIGHT BUT NO SHADOWS (SCREEN LIGHTS)
//    else if(render_style == 5)
//    {
//        vec3 diffuse = vec3(0);
//        for(int i=0; i<screen_lights.length(); i++){
//            diffuse += diffuse_spot_lighting(screen_lights[i]).rgb;
//        }
////        vec3 light_intensity = screen_lights[0].color.rgb * screen_lights[0].intensity;
////        vec3 ambient = material.ambient_coef.rgb * light_intensity * 0.5;
//        vec3 textura = texture(object_texture, texture_coord).rgb;
//        frag_color = vec4((diffuse/* + ambient*/) * textura, material.alpha);
////        frag_color = vec4(diffuse, material.alpha);
////        vec3 video_mapping = texture(screen_texture, texture_coord).rgb;
////        frag_color = vec4((diffuse/* + ambient*/) * (textura + video_mapping), material.alpha);
////        frag_color = vec4(video_mapping, 1);
//    }
}
