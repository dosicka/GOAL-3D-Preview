#ifndef SCENEMOVEMENT_H
#define SCENEMOVEMENT_H

#include <QObject>
#include <QVector3D>
#include <glm/glm.hpp>

class SceneCamera
{
    glm::vec3 m_position = glm::vec3(0); // toto je pozice - BOD
    glm::vec3 m_up = glm::vec3(0); // toto je VEKTOR
    glm::vec3 m_forward = glm::vec3(0); // toto je VEKTOR | nesmí být na začátku (0,0,0), jinak nebude fungovat zoom dokud nepohnu s kamerou
    float m_fovy = 0; // 90 je moc -> deformace objektů na orajích obrazovky, 45 vypadá dobře
    float m_zNear = 0; // když je moc malé číslo (např. 0.1), tak jsou problémy se z-fightingem (depth test)
    float m_zFar = 0;

public:

    SceneCamera();

    void moveCamera(float distanceX, float distanceY); // rotate camera arround center view point - changes the position of camera
    void rotateCamera(float distanceX, float distanceY); // rotate camera arround with stable position
    void translateCamera(float distanceX, float distanceY); // translate camera - move horizontaly and verticaly
    void zoomCamera(float distanceZ);

    glm::vec3 positionAfterMove(float distanceX, float distanceY);
    glm::vec3 positionAfterTranslate(float distanceX, float distanceY);
    glm::vec3 positionAfterZoom(float distanceZ);
    void updateVectorsAfterMove(float distanceX, float distanceY);

    void setPosition(float x, float y, float z);
    void setUpVector(float x, float y, float z);
    void setForwardVector(float x, float y, float z);
    void setFovy(float fovy);
    void setNearClipPlane(float clip);
    void setFarClipPlane(float clip);

    glm::vec3 position();
    glm::vec3 up();
    glm::vec3 forward();
    glm::vec3 eyeForwardVector();
    float fieldOfView();
    float nearClipPlane();
    float farClipPlane();

private:

    glm::vec3 rotateVector(glm::vec3 vec, glm::vec3 axis, float theta);
};

#endif // SCENEMOVEMENT_H
