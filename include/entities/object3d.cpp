#include "object3d.h"

Object3D::Object3D()
{
}

Object3D::~Object3D()
{
    // delete buffers
    glDeleteBuffers(1, &m_vertexbuffer);
    glDeleteBuffers(1, &m_normalbuffer);
    glDeleteBuffers(1, &m_uvbuffer);
}

bool Object3D::loadObject(QString pathToFile)
{
    QDir currentDir(QCoreApplication::applicationDirPath());
    QFile file(currentDir.absoluteFilePath(pathToFile));
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "[ERROR]\t Object3D::loadObject - unable to open file: " << file.fileName();
        return false;
    }

    // it is important so the vertices are in correct order, order of vertices gives normal vector
    // normal vector of face implies which side of triangle is visible (if GL_CULL_FACE enabled)
    QVector<glm::vec4> vertices;
    QVector<glm::vec3> vertexNormals;
    QVector<glm::vec2> textureCoords;
    QVector<uint> vertexIndices, textureIndices, normalIndices;

    QTextStream stream(&file);

    bool minMaxInitialized = false;

    while (!stream.atEnd()) {
        QString line = stream.readLine();
        if(line.isEmpty()){
            continue;
        }

        QStringList args = line.split(' ', QString::SkipEmptyParts);

        // load vertices
        if(args.at(0) == "v") {
            qreal x = args.at(1).toDouble();
            qreal y = args.at(2).toDouble();
            qreal z = args.at(3).toDouble();
            glm::vec4 vertex(x, y, z, 1.0f);
            vertices.append(vertex);

            if(!minMaxInitialized){
                m_minX = m_maxX = x;
                m_minY = m_maxY = y;
                m_minZ = m_maxZ = z;
                minMaxInitialized = true;
            }
            if(x > m_maxX){
                m_maxX = x;
            } else if(x < m_minX){
                m_minX = x;
            }
            if(y > m_maxY){
                m_maxY = y;
            } else if(y < m_minY){
                m_minY = y;
            }
            if(z > m_maxZ){
                m_maxZ = z;
            } else if(z < m_minZ){
                m_minZ = z;
            }
        }
        // load normals
        else if(args.at(0) == "vn"){
            qreal x = args.at(1).toDouble();
            qreal y = args.at(2).toDouble();
            qreal z = args.at(3).toDouble();
            glm::vec3 normal(x, y, z);
            vertexNormals.append(normal);
        }
        // load texture coordinates
        else if(args.at(0) == "vt"){
            qreal u = args.at(1).toDouble();
            qreal v = args.at(2).toDouble();
            glm::vec2 textureCoord(u, v);
            textureCoords.append(textureCoord);
        }
        // load faces
        else if(args.at(0) == "f"){
            for(int i=1; i<(args.size()); i++){
                QStringList indices = args.at(i).split('/');
                uint ind = 0;
                if(indices.size() >= 1){
                    ind = indices.at(0).toUInt();
                    if(ind > 0){
                        vertexIndices.append(ind);
                    }
                }
                if(indices.size() >= 2){
                    ind = indices.at(1).toUInt();
                    if(ind > 0){
                        textureIndices.append(ind);
                    }
                }
                if(indices.size() >= 3){
                    ind = indices.at(2).toUInt();
                    if(ind > 0){
                        normalIndices.append(ind);
                    }
                }
            }
        }
    }

    // save vertices in right order according to faces
    if(vertices.isEmpty()){
        qDebug() << "Object3D::loadObject - no vertices" << file.fileName();
        return false; // no object without vertices
    }
    for(int i=0; i<vertexIndices.size(); i++){
        if(vertexIndices.at(i) > 0 && vertexIndices.at(i) <= static_cast<uint>(vertices.size())){
            m_vertices.append(vertices.at(vertexIndices.at(i)-1));
        } else {
            qDebug() << "ObjectLoader::loadObject - corrupted vertices" << file.fileName();
            break;
        }
    }

    // save texture coordinates in right order according to faces
    if(textureCoords.isEmpty()){
        qDebug() << "Object3D::loadObject - no texture coordinates" << file.fileName();
    } else {
        for(int i=0; i<textureIndices.size(); i++){
            if(textureIndices.at(i) > 0 && textureIndices.at(i) <= static_cast<uint>(textureCoords.size())){
                m_textureCoords.append(textureCoords.at(textureIndices.at(i)-1));
            } else {
                qDebug() << "ObjectLoader::loadObject - corrupted texture coordinates" << file.fileName();
                break;
            }
        }
    }

    // save normals in right order according to faces
    if(vertexNormals.isEmpty()){
        qDebug() << "Object3D::loadObject - no vertex normals" << file.fileName();
    } else {
        for(int i=0; i<normalIndices.size(); i++){
            if(normalIndices.at(i) > 0 && normalIndices.at(i) <= static_cast<uint>(vertexNormals.size())){
                m_vertexNormals.append(vertexNormals.at(normalIndices.at(i)-1));
            } else {
                qDebug() << "ObjectLoader::loadObject - corrupted normals" << file.fileName();
                break;
            }
        }
    }

    m_xSize = m_maxX - m_minX;
    m_ySize = m_maxY - m_minY;
    m_zSize = m_maxZ - m_minZ;

    qreal max = 0;
    m_xSize > m_ySize ? max = m_xSize : max = m_ySize;
    max < m_zSize ? max = m_zSize : 0;

    // the smaller number is set here, the smaller is the object in scene
    m_scaleCoef = 100.0f / max;

    file.close();
    return true;
}


QVector<glm::vec4> Object3D::vertices(){
    return m_vertices;
}

QVector<glm::vec3> Object3D::vertexNormals(){
    return m_vertexNormals;
}

QVector<glm::vec2> Object3D::textureCoords(){
    return m_textureCoords;
}

float Object3D::minX(){
    return m_minX;
}

float Object3D::maxX(){
    return m_maxX;
}

float Object3D::minY(){
    return m_minY;
}

float Object3D::maxY(){
    return m_maxY;
}

float Object3D::minZ(){
    return m_minZ;
}

float Object3D::maxZ(){
    return m_maxZ;
}

float Object3D::xSize(){
    return m_xSize;
}

float Object3D::ySize(){
    return m_ySize;
}

float Object3D::zSize(){
    return m_zSize  ;
}

float Object3D::scaleCoeficient(){
    return m_scaleCoef;
}

void Object3D::setMaterial(Object3D::Material material){
    m_material = material;
}

Object3D::Material Object3D::material(){
    return m_material;
}


void Object3D::bindVertices()
{
    // Generate 1 buffer, put the resulting identifier in vertexbuffer
    glGenBuffers(1, &m_vertexbuffer);
    // The following commands will talk about our 'vertexbuffer' buffer
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);
    // Give our vertices to OpenGL.
    if(!m_vertices.isEmpty()){
        glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(glm::vec4), &m_vertices[0], GL_STATIC_DRAW);
    }
}

void Object3D::bindNormals()
{
    glGenBuffers(1, &m_normalbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_normalbuffer);
    if(!m_vertexNormals.isEmpty()){
        glBufferData(GL_ARRAY_BUFFER, m_vertexNormals.size() * sizeof(glm::vec3), &m_vertexNormals[0], GL_STATIC_DRAW);
    }
}

void Object3D::bindTextureUV()
{
    glGenBuffers(1, &m_uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_uvbuffer);
    if(!m_textureCoords.isEmpty()){
        glBufferData(GL_ARRAY_BUFFER, m_textureCoords.size() * sizeof(glm::vec2), &m_textureCoords[0], GL_STATIC_DRAW);
    }
}

void Object3D::bindGeometry()
{
    bindVertices();
    bindNormals();
    bindTextureUV();
}


void Object3D::passVerticesToShader()
{
    // 1st attribute buffer : vertices - this block of code must be implemented for each buffer passed to shader
    if(m_vertices.isEmpty()) {
        return;
    }
    // glEnableVertexAttribArray(0) - set buffer n. 0 (vertices) enabled in shader
    // if no vertices in buffer -> shader program crashes,
    // therefore must be checked if there are vertices in m_vertices before buffer is enabled
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);
    glVertexAttribPointer(
       0,                  // attribute 0, must match the layout number in the shader.
       4,                  // size of attribute 4 for vec4
       GL_FLOAT,           // type of each value in attribute in vec4 are floats
       GL_FALSE,           // normalized?
       0,                  // stride (offset between every couple of values)
       (void*)0            // array buffer offset at beginning
    );
}

void Object3D::passNomalsToShader()
{
    // 2nd attribute buffer : normals
    if(m_vertexNormals.isEmpty()) {
        return;
    }
    glEnableVertexAttribArray(1);
    glBindBuffer(GL_ARRAY_BUFFER, m_normalbuffer);
    glVertexAttribPointer(
        1,          // attribute
        3,          // size
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0    // array buffer offset
    );
}

void Object3D::passTextureUVToShader()
{
    // 3nd attribute buffer : UVs
    if(m_textureCoords.isEmpty()) {
        return;
    }
    glEnableVertexAttribArray(2);
    glBindBuffer(GL_ARRAY_BUFFER, m_uvbuffer);
    glVertexAttribPointer(
        2,          // attribute. must match the layout in the shader.
        2,          // size : U+V => 2
        GL_FLOAT,   // type
        GL_FALSE,   // normalized?
        0,          // stride
        (void*)0     // array buffer offset
    );
}


void Object3D::draw()
{
    passVerticesToShader();
    passNomalsToShader();
    passTextureUVToShader();
    // Draw the triangle - musí byt až po tom co jsou do shaderu nahrány všechny buffery, ne jen vertexy
    // Starting from vertex 0 - number of vertices to draw
    if(!m_vertices.isEmpty())
    {
        glDrawArrays(GL_TRIANGLES, 0, m_vertices.size());
    }
    // toto udělá disable bufferů 0-vertices, 1-normals, 2-uvCoords v shaderu, aby se nerenderovali
    // je lepší to po jejich vykreslení udělat a před kreslením opět povolit (to se dělá v pass.. funkcích),
    // aby aplikace nespadla, kdyby se měnili vertexy, normály, uvCoord za běhu
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

void Object3D::drawVertices()
{
    passVerticesToShader();
    if(!m_vertices.isEmpty())
    {
        glDrawArrays(GL_TRIANGLES, 0, m_vertices.size());
    }
    glDisableVertexAttribArray(0);
}

void Object3D::drawNormals()
{
    // draw normals - consumes CPU a lot
    glLineWidth(1.0);
    glBegin(GL_LINES);
    for (int i=0; i<m_vertices.size(); i++)
    {
        glVertex3f(m_vertices.at(i).x, m_vertices.at(i).y, m_vertices.at(i).z);
        glVertex3f(m_vertices.at(i).x + 20*m_vertexNormals.at(i).x,
                   m_vertices.at(i).y + 20*m_vertexNormals.at(i).y,
                   m_vertices.at(i).z + 20*m_vertexNormals.at(i).z);
    }
    glEnd();
}


void Object3D::genMaterialBuffer()
{
    glGenBuffers(1, &m_uboMaterial);
}

void Object3D::passMaterialToShader(int bindingIndex)
{
    if(m_uboMaterial == 0){
        return;
    }
    glBindBuffer(GL_UNIFORM_BUFFER, m_uboMaterial); // bind
    glBufferData(GL_UNIFORM_BUFFER, sizeof(m_material), &m_material, GL_STATIC_DRAW);
    glBindBufferBase(GL_UNIFORM_BUFFER, bindingIndex, m_uboMaterial);
    glBindBuffer(GL_UNIFORM_BUFFER, 0); // unbind
}
