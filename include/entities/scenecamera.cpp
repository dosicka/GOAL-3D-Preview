#include "scenecamera.h"

SceneCamera::SceneCamera()
{
}

void SceneCamera::moveCamera(float distanceX, float distanceY)
{
    glm::vec3 up = glm::vec3(0,1,0);
    glm::vec3 camera_right = glm::normalize(glm::cross(m_forward, m_up));
    // move in X axis
    double thetaX = 1.0 * distanceX;
    m_forward = glm::normalize(rotateVector(m_forward, up, thetaX));
    m_up = glm::normalize(rotateVector(m_up, up, thetaX));
    m_position = rotateVector(m_position, up, thetaX);
    // move in Y axis
    double thetaY = 1.0 * distanceY;
    m_forward = glm::normalize(rotateVector(m_forward, camera_right, thetaY));
    m_up = glm::normalize(rotateVector(m_up, camera_right, thetaY));
    m_position = rotateVector(m_position, camera_right, thetaY);
}

void SceneCamera::rotateCamera(float distanceX, float distanceY)
{
    glm::vec3 up = glm::vec3(0,1,0);
    glm::vec3 camera_right = glm::normalize(glm::cross(m_forward, m_up));
    // rotate in X axis
    double thetaX = 1.0 * distanceX;
    m_forward = glm::normalize(rotateVector(m_forward, up, thetaX));
    m_up = glm::normalize(rotateVector(m_up, up, thetaX));
    // rotate in Y axis
    double thetaY = 1.0 * distanceY;
    m_forward = glm::normalize(rotateVector(m_forward, camera_right, thetaY));
    m_up = glm::normalize(rotateVector(m_up, camera_right, thetaY));
}

void SceneCamera::translateCamera(float distanceX, float distanceY)
{
    glm::vec3 right = glm::cross(m_forward, m_up);
    m_position -= right * distanceX * 50.0f;
    m_position += m_up * distanceY * 50.0f;
}

void SceneCamera::zoomCamera(float distanceZ)
{
    m_position += m_forward * (distanceZ * 0.03f);
}


glm::vec3 SceneCamera::positionAfterMove(float distanceX, float distanceY)
{
    glm::vec3 position = m_position;
    glm::vec3 up = glm::vec3(0,1,0);
    glm::vec3 camera_right = glm::normalize(glm::cross(m_forward, m_up));

    // move in X axis
    double thetaX = 1.0 * distanceX;
    position = rotateVector(position, up, thetaX);
    // move in Y axis
    double thetaY = 1.0 * distanceY;
    position = rotateVector(position, camera_right, thetaY);

    return position;
}

glm::vec3 SceneCamera::positionAfterTranslate(float distanceX, float distanceY)
{
    glm::vec3 position = m_position;
    glm::vec3 right = glm::cross(m_forward, m_up);

    position -= right * distanceX * 50.0f;
    position += m_up * distanceY * 50.0f;

    return position;
}

glm::vec3 SceneCamera::positionAfterZoom(float distanceZ)
{
    glm::vec3 position = m_position;
    position += m_forward * (distanceZ * 0.03f);
    return position;
}

void SceneCamera::updateVectorsAfterMove(float distanceX, float distanceY)
{
    glm::vec3 up = glm::vec3(0,1,0);
    glm::vec3 camera_right = glm::normalize(glm::cross(m_forward, m_up));
    // move in X axis
    double thetaX = 1.0 * distanceX;
    m_forward = glm::normalize(rotateVector(m_forward, up, thetaX));
    m_up = glm::normalize(rotateVector(m_up, up, thetaX));
    // move in Y axis
    double thetaY = 1.0 * distanceY;
    m_forward = glm::normalize(rotateVector(m_forward, camera_right, thetaY));
    m_up = glm::normalize(rotateVector(m_up, camera_right, thetaY));
}


void SceneCamera::setPosition(float x, float y, float z)
{
    m_position = glm::vec3(x, y, z);
}

void SceneCamera::setUpVector(float x, float y, float z)
{
    m_up = glm::normalize(glm::vec3(x, y, z));
}

void SceneCamera::setForwardVector(float x, float y, float z)
{
    m_forward = glm::normalize(glm::vec3(x, y, z));
}

void SceneCamera::setFovy(float fovy)
{
    m_fovy = fovy;
}

void SceneCamera::setNearClipPlane(float clip)
{
    m_zNear = clip;
}

void SceneCamera::setFarClipPlane(float clip)
{
    m_zFar = clip;
}


glm::vec3 SceneCamera::position()
{
    return m_position;
}

glm::vec3 SceneCamera::up()
{
    return m_up;
}

glm::vec3 SceneCamera::forward()
{
    return m_forward;
}

glm::vec3 SceneCamera::eyeForwardVector()
{
    return m_position + m_forward;
}

float SceneCamera::fieldOfView()
{
    return m_fovy;
}

float SceneCamera::nearClipPlane()
{
    return m_zNear;
}

float SceneCamera::farClipPlane()
{
    return m_zFar;
}


glm::vec3 SceneCamera::rotateVector(glm::vec3 vec, glm::vec3 axis, float theta)
{
    //rotate vector vec around the line defined by axis through the origin by theta degrees.
    const double cos_theta = cos(theta);
    const double dot = glm::dot(vec, axis);
    glm::vec3 cross = glm::cross(vec, axis);
    glm::vec3 result;
    result.x = (vec.x * cos_theta) + (axis.x * dot * (1.0-cos_theta)) - (cross.x * sin(theta));
    result.y = (vec.y * cos_theta) + (axis.y * dot * (1.0-cos_theta)) - (cross.y * sin(theta));
    result.z = (vec.z * cos_theta) + (axis.z * dot * (1.0-cos_theta)) - (cross.z * sin(theta));
    return result;
}
