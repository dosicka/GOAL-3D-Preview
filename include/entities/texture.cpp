#include "texture.h"

Texture::Texture()
{
}

Texture::~Texture()
{
    if(m_data != nullptr){
        delete m_data;
        m_data = nullptr;
    }
    glDeleteTextures(1, &m_textureID);
}

void Texture::loadTexture(QString path)
{
    QDir currentDir(QCoreApplication::applicationDirPath());
    QString p = currentDir.absoluteFilePath(path);
    QImage img(p);
    setTexture(img);
}

void Texture::setTexture(QImage texture)
{
    // m_data must be buffer, not only pointer on buffered data as:
    // unsigned char data[texture.byteCount()];
    // memcpy(data, texture.bits(), texture.byteCount());
    // m_data = data;
    // - this does not work

    if(m_texture != texture)
    {
        if(m_texture.byteCount() != texture.byteCount()){
            // nemělo by se používat new a delete, ale něco jiného (Michalova přednáška)
            delete m_data;
            m_data = new unsigned char[texture.byteCount()];
        }

        m_frameID = -1;
        m_texture = texture;
        m_width = texture.width();
        m_height = texture.height();

        if(texture.format() != QImage::Format_RGBA8888){
            texture = texture.convertToFormat(QImage::Format_RGBA8888);
        }
        // flip image because OpenGL UV coordinest origin is bottom left
        QImage img = texture.mirrored();
        memcpy(m_data, img.bits(), img.byteCount());
    }
}

void Texture::setVideoFrame(QImage frame, int frameID)
{
    if(m_frameID != frameID)
    {
        if(m_texture.byteCount() != frame.byteCount()){
            delete m_data;
            m_data = new unsigned char[frame.byteCount()];
        }

        m_frameID = frameID;
        m_texture = frame;
        m_width = frame.width();
        m_height = frame.height();

        if(frame.format() != QImage::Format_RGBA8888){
            frame = frame.convertToFormat(QImage::Format_RGBA8888);
        }
        // flip image because OpenGL UV coordinest origin is bottom left
        QImage img = frame.mirrored();
        memcpy(m_data, img.bits(), img.byteCount());
    }
}

QImage Texture::texture()
{
    return m_texture;
}

unsigned int Texture::width()
{
    return m_width;
}

unsigned int Texture::height()
{
    return m_height;
}

unsigned char *Texture::data()
{
    return m_data;
}

void Texture::bindToShader()
{
    glGenTextures(1, &m_textureID);
    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, m_textureID);
    // Give the image to OpenGL
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &m_data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//    // ... nice trilinear filtering ...
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
//    // ... which requires mipmaps. Generate them automatically.
//    glGenerateMipmap(GL_TEXTURE_2D);
}

void Texture::passToShader(GLuint locationInShader, GLenum textureUnitNumber)
{
    // reinterpred GLenum as integer number for texture
    int textureNumber = static_cast<int>(textureUnitNumber-GL_TEXTURE0);
    // texture unit number can not be higher than max number of textures for this implementation
    if(textureNumber >= GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS){
       return;
    }
    // Bind our texture in Texture Unit with number textureUnitNumber (0, 1, ....)
    glActiveTexture(textureUnitNumber);
    glBindTexture(GL_TEXTURE_2D, m_textureID);
    // update content of texture (for video playback)
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_data);
    // Set our "textureSampler" sampler to use Texture Unit with number in textureNumber (same as textureUnitNumber)
    glUniform1i(locationInShader, textureNumber);
    // unbind - does not work
    //    glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::setMantinelIntensity(float intensity)
{
    m_mantinelIntensity = intensity/ 5.0f;
}

void Texture::blendMantinelTexture(QImage image)
{
    // přeformátovat ???
//    QImage::Format_ARGB32_Premultiplied;    // nejlepší pro compisiting
//    if(frame.format() != QImage::Format_RGBA8888){ // musí byt pro texturu
//        frame = frame.convertToFormat(QImage::Format_RGBA8888);
//    }

    QImage resultImage = QImage(m_width, m_height, QImage::Format_ARGB32_Premultiplied);
//    QImage tex = m_texture.convertToFormat(QImage::Format_ARGB32_Premultiplied);
//    QImage tex = m_texture.convertToFormat(QImage::Format_RGB32);
//    QImage tex("hockey-field.png");
//    image = image.convertToFormat(QImage::Format_ARGB32_Premultiplied);
    image = image.scaled(885, 60, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);

    float x = (m_texture.width() - image.width()) / 2;
    float y = m_texture.height() - image.height();

    QPainter p;
    p.begin(&image);
    p.setCompositionMode(QPainter::CompositionMode_DestinationIn);
    p.fillRect(image.rect(), QColor(0, 0, 0, m_mantinelIntensity * 250));
    p.end();

//    p.begin(&tex);
//    p.setCompositionMode(QPainter::CompositionMode_DestinationIn);
//    p.fillRect(tex.rect(), QColor(0, 0, 0, 100));
//    p.end();


//    p.begin(&m_texture);
//    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
//    p.drawImage(x, y, image);
//    p.end();


    p.begin(&resultImage);
    p.setCompositionMode(QPainter::CompositionMode_Source);
//    p.fillRect(resultImage.rect(), Qt::transparent);
    p.fillRect(resultImage.rect(), Qt::white);
    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    p.drawImage(0, 0, m_texture);
//    p.drawImage(0, 0, tex);
    p.setCompositionMode(QPainter::CompositionMode_SourceOver);
    p.drawImage(x, y, image);
//    p.setCompositionMode(QPainter::CompositionMode_DestinationOver);
//    p.fillRect(resultImage.rect(), Qt::white);
    p.end();

//    m_texture = resultImage;
    resultImage = resultImage.convertToFormat(QImage::Format_RGBA8888);

    memcpy(m_data, resultImage.bits(), resultImage.byteCount());
}

