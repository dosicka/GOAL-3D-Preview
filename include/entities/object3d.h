#ifndef OBJECT3D_H
#define OBJECT3D_H

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <glm/glm.hpp>

#include <QTextStream>
#include <QObject>
#include <QVector>
#include <QDir>
#include <QFile>
#include <QCoreApplication>
#include <QDebug>


class Object3D
{
public:

    struct Material {
        glm::vec4 ambientCoef = glm::vec4(0);
        glm::vec4 diffuseCoef = glm::vec4(0);
        glm::vec4 specularCoef = glm::vec4(0);
        float shinesCoef = 0;
        float alphaChanel = 0;
    };

private:

    QVector<glm::vec4> m_vertices;
    QVector<glm::vec3> m_vertexNormals;
    QVector<glm::vec2> m_textureCoords;

    GLuint m_vertexbuffer;
    GLuint m_normalbuffer;
    GLuint m_uvbuffer;

    float m_minX = 0;
    float m_maxX = 0;
    float m_minY = 0;
    float m_maxY = 0;
    float m_minZ = 0;
    float m_maxZ = 0;
    float m_xSize = 0;
    float m_ySize = 0;
    float m_zSize = 0;
    float m_scaleCoef = 1.0;

    Material m_material;
    GLuint m_uboMaterial = 0;

public:

    Object3D();
    ~Object3D();

    bool loadObject(QString pathToFile);

    QVector<glm::vec4> vertices();
    QVector<glm::vec3> vertexNormals();
    QVector<glm::vec2> textureCoords();

    float minX();
    float maxX();
    float minY();
    float maxY();
    float minZ();
    float maxZ();
    float xSize();
    float ySize();
    float zSize();
    float scaleCoeficient();

    void setMaterial(Material material);
    Material material();

    // BIND BUFFERS TO SHADERS
    // když nejsou žádný vertexy, normály, text. souřadnice v QVectoru, tak buffer nebindovat
    // a neloadovat do shaderu, na prázdným bufferu to pak v shederu spadne, ale pokud ho nebindnu, tak ne.
    // Hlavní je asi funkce glEnableVertexAttribArray(0), ta povoluje buffer v shaderu, dyž ho nepovolí, tak to nespadne
    // proto je v bind.. i pass.. funkcích vždy ošetřeno, když QVector.isEmpty(), tak se nenahrají do shaderu
    void bindVertices();
    void bindNormals();
    void bindTextureUV();
    void bindGeometry(); // bind vertices, normals, texture UV (all previous)

    void passVerticesToShader();
    void passNomalsToShader();
    void passTextureUVToShader();

    void draw();
    void drawVertices();
    void drawNormals();

    void genMaterialBuffer();
    void passMaterialToShader(int bindingIndex); // bindingIndex is number of binding (same as in shader)
};

#endif // OBJECT3D_H
