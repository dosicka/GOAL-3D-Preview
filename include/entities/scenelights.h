#ifndef LIGHTWRAPPER_H
#define LIGHTWRAPPER_H

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <glm/glm.hpp>

#include <QVector>
#include <QCoreApplication>
#include <QIODevice>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include "include/entities/object3d.h"


class SceneLights
{
public:

    // by layout std430 in glsl must be 4 floats alligned and vec4 is required instead of vec3
    struct SceneLight{
        glm::vec4 position = glm::vec4(0, 0, 0, 1); // last number always 1 for point
        glm::vec4 direction = glm::vec4(0); // last number always 0 for vector
        glm::vec4 color = glm::vec4(0); // color value range 0.0 - 1.0
        float intensity = 0; // intensity value range 0.0 - 3.0 (for current scene, can by higher according to materials, bcs it multiplies them)
        float cutoff = 0; // Cutoff angle (between 0 and 90) - jen u spot light úhel za kterým světlo už vůbec nesvítí (odchylka od normály -> max 90°)
        float angleAttenuation = 0; // Angular attenuation exponent - rychlost úbytku světla od středu do stran, jen u spot light (menší číslo = pomalejší úbytek)
        float distanceAttenuation = 0; // can go above 1.0 (when more light, must increase, not decrease) úbytek světla ve vzdálenosti (menší číslo = pomalejší úbytek)
                                       // pro lineářní úbytek hodnoty kolem 1, pro kvadratický kolem 0.01 - 0.1
    };

    enum LevelOfDetails{
           low = 0, // nejsou světla obrazovek
           medium = 1,  // menší počet světel, pro kostku jsou jen main screen lights a používají se i pro top ring a bottom ring
           high = 2 // větší počet světel, pro každou obrazovku zvlášť
       };

private:

    LevelOfDetails m_lod = LevelOfDetails::low;
    QJsonObject m_jsonObject; // JSON object with light settings loaded from config file

    QVector<SceneLight> m_lights;
    QVector<SceneLight> m_screenLights;

    QVector<int> m_mainScreenIndices;
    QVector<int> m_topRingIndices;
    QVector<int> m_bottomRingIndices;
    QVector<int> m_mantinelIndices;
    QVector<int> m_bigRingIndices;
    QVector<int> m_mappingIndices;

    float m_intensityLights = 0;
    float m_intensityMainScreen = 0;
    float m_intensityTopRing = 0;
    float m_intensityBottomRing = 0;
    float m_intensityMantinelScreen = 0;
    float m_intensityBigRing = 0;
    float m_intensityMapping = 0;

    GLuint m_ssboLights = 0;
    GLuint m_ssboScreens = 0;

public:

    SceneLights();
    ~SceneLights();

    void setLevelOfDetails(LevelOfDetails detailsLevel); // must be called before creating lights
    void loadLightConfigFile(QString filePath);

    SceneLight lightAt(int index) const;
    SceneLight screenLightAt(int index) const;

    float intensityLights();
    float intensityMainScreen();
    float intensityTopRing();
    float intensityBottomRing();
    float intensityMantinelScreen();
    float intensityBigRing();
    float intensityMapping();

    void setIntensityLights(float intensity);
    void setIntensityMainScreen(float intensity);
    void setIntensityTopRing(float intensity);
    void setIntensityBottomRing(float intensity);
    void setIntensityMantinelScreen(float intensity);
    void setIntensityBigRing(float intensity);
    void setIntensityMapping(float intensity);

    void genSSBOBufferLights();
    void genSSBOBufferScreenLights();
    void passLightsToShader(int bindingIndex);
    void passScreenLightsToShader(int bindingIndex);

    void createLights(Object3D lightsObject);
    void createMainScreenLights(Object3D mainScreens);
    void createTopRingLights(Object3D topRing);
    void createBottomRingLights(Object3D bottomRing);
    void createMantinelLights(Object3D mantinelScreen);
    void createBigRingLights(Object3D bigRing);
    void createMappingLights(Object3D lightsObject);

private:

    void setIntensityMainScreenLights(float intensity);
    void setIntensityTopRingLights(float intensity);
    void setIntensityBottomRingLights(float intensity);
    void setIntensityMantinelScreenLights(float intensity);
    void setIntensityBigRingLights(float intensity);
    void setIntensityMappingLights(float intensity);

    SceneLight loadLight(QString lightTag);
};

#endif // LIGHTWRAPPER_H
