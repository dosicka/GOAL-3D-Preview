#ifndef TEXTURE_H
#define TEXTURE_H

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>

#include <QObject>
#include <QDir>
#include <QCoreApplication>
#include <QImage>
#include <QPainter>


class Texture
{
    GLuint m_textureID = 0; // -2 = black QImage | -1 = texture set with setTexture function | 0,1,2,... = frames of video set with setVideoFrame function
    QImage m_texture;
    int m_frameID = -1;
    unsigned char * m_data = nullptr;
    unsigned int m_width = 0;
    unsigned int m_height = 0;

    float m_mantinelIntensity = 0;

public:

    Texture();
    ~Texture();

    void loadTexture(QString path);
    void setTexture(QImage texture);

    QImage texture();
    unsigned int width();
    unsigned int height();
    unsigned char* data();

    void bindToShader();
    void passToShader(GLuint locationInShader, GLenum textureUnitNumber);

    void setMantinelIntensity(float intensity);
    void blendMantinelTexture(QImage image);

public slots:

    void setVideoFrame(QImage frame, int frameID);
};

#endif // TEXTURE_H
