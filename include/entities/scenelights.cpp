#include "scenelights.h"

SceneLights::SceneLights()
{
}

SceneLights::~SceneLights()
{
    m_lights.clear();
    m_lights.squeeze();

    m_screenLights.clear();
    m_screenLights.squeeze();

    m_mainScreenIndices.clear();
    m_mainScreenIndices.squeeze();

    m_topRingIndices.clear();
    m_topRingIndices.squeeze();

    m_bottomRingIndices.clear();
    m_bottomRingIndices.squeeze();

    m_mantinelIndices.clear();
    m_mantinelIndices.squeeze();

    m_bigRingIndices.clear();
    m_bigRingIndices.squeeze();

    glDeleteBuffers(1, &m_ssboLights);
    glDeleteBuffers(1, &m_ssboScreens);
}

void SceneLights::setLevelOfDetails(SceneLights::LevelOfDetails detailsLevel)
{
    m_lod = detailsLevel;
}

void SceneLights::loadLightConfigFile(QString filePath)
{
    QDir currentDir(QCoreApplication::applicationDirPath());
    QFile file(currentDir.absoluteFilePath(filePath));

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "[ERROR]\t SceneLights::loadLightConfigFile - unable to open file: " << file.fileName();
        return;
    }

    QJsonDocument document =  QJsonDocument::fromJson(file.readAll());
    if(document.isNull()) {
        qDebug() << "[ERROR]\t SceneLights::loadLightConfigFile - corrupted (invalid JSON) config file " << file.fileName();
        return;
    }

    file.close();
    m_jsonObject = document.object();
}


SceneLights::SceneLight SceneLights::lightAt(int index) const
{
    if(index < 0 || index >= m_lights.size()){
        return SceneLight();
    }
    return m_lights.at(index);
}

SceneLights::SceneLight SceneLights::screenLightAt(int index) const
{
    if(index < 0 || index >= m_screenLights.size()){
        return SceneLight();
    }
    return m_screenLights.at(index);
}


float SceneLights::intensityLights()
{
    return m_intensityLights;
}

float SceneLights::intensityMainScreen()
{
    return m_intensityMainScreen;
}

float SceneLights::intensityTopRing()
{
    return m_intensityTopRing;
}

float SceneLights::intensityBottomRing()
{
    return m_intensityBottomRing;
}

float SceneLights::intensityMantinelScreen()
{
    return m_intensityMantinelScreen;
}

float SceneLights::intensityBigRing()
{
    return m_intensityBigRing;
}

float SceneLights::intensityMapping()
{
    float intensity = m_intensityMapping - m_intensityLights;
    if(intensity < 0){
        return 0;
    }
    return intensity;
}


void SceneLights::setIntensityLights(float intensity)
{
    m_intensityLights = intensity < 0 ? 0 : intensity;

    for(int i=0; i<m_lights.size(); i++) {
        m_lights[i].intensity = m_intensityLights;
    }
    // set also mapping intensity
    float intens = m_intensityMapping - m_intensityLights;
    if(intens < 0){
        intens = 0;
    }
    setIntensityMappingLights(intens);
}

void SceneLights::setIntensityMainScreen(float intensity)
{
    m_intensityMainScreen = intensity < 0 ? 0 : intensity;

    if(m_lod == LevelOfDetails::medium){
        setIntensityMainScreenLights((m_intensityMainScreen + m_intensityTopRing*0.5 + m_intensityBottomRing*0.5));
    } else {
        setIntensityMainScreenLights(m_intensityMainScreen);
    }
}

void SceneLights::setIntensityTopRing(float intensity)
{
    m_intensityTopRing = intensity < 0 ? 0 : intensity;

    if(m_lod == LevelOfDetails::medium){
        setIntensityMainScreenLights((m_intensityMainScreen + m_intensityTopRing*0.5 + m_intensityBottomRing*0.5));
    } else {
        setIntensityTopRingLights(m_intensityTopRing);
    }
}

void SceneLights::setIntensityBottomRing(float intensity)
{
    m_intensityBottomRing = intensity < 0 ? 0 : intensity;

    if(m_lod == LevelOfDetails::medium){
        setIntensityMainScreenLights((m_intensityMainScreen + m_intensityTopRing*0.5 + m_intensityBottomRing*0.5));
    } else {
        setIntensityBottomRingLights(m_intensityBottomRing);
    }
}

void SceneLights::setIntensityMantinelScreen(float intensity)
{
    m_intensityMantinelScreen = intensity < 0 ? 0 : intensity;
    setIntensityMantinelScreenLights(m_intensityMantinelScreen);
}

void SceneLights::setIntensityBigRing(float intensity)
{
    m_intensityBigRing = intensity < 0 ? 0 : intensity;
    setIntensityBigRingLights(m_intensityBigRing);
}

void SceneLights::setIntensityMapping(float intensity)
{
    m_intensityMapping = intensity < 0 ? 0 : intensity;

    float intens = m_intensityMapping - m_intensityLights;
    if(intens < 0){
        intens = 0;
    }
    setIntensityMappingLights(intens);
}


void SceneLights::setIntensityMainScreenLights(float intensity)
{
    for(int i=0; i<m_mainScreenIndices.size(); i++) {
        int index = m_mainScreenIndices.at(i);
        if(index >= 0 && index < m_screenLights.size()){
            m_screenLights[index].intensity = intensity;
        }
    }
}

void SceneLights::setIntensityTopRingLights(float intensity)
{
    for(int i=0; i<m_topRingIndices.size(); i++) {
        int index = m_topRingIndices.at(i);
        if(index >= 0 && index < m_screenLights.size()){
            m_screenLights[index].intensity = intensity;
        }
    }
}

void SceneLights::setIntensityBottomRingLights(float intensity)
{
    for(int i=0; i<m_bottomRingIndices.size(); i++) {
        int index = m_bottomRingIndices.at(i);
        if(index >= 0 && index < m_screenLights.size()){
            m_screenLights[index].intensity = intensity;
        }
    }
}

void SceneLights::setIntensityMantinelScreenLights(float intensity)
{
    for(int i=0; i<m_mantinelIndices.size(); i++) {
        int index = m_mantinelIndices.at(i);
        if(index >= 0 && index < m_screenLights.size()){
            m_screenLights[index].intensity = intensity;
        }
    }
}

void SceneLights::setIntensityBigRingLights(float intensity)
{
    for(int i=0; i<m_bigRingIndices.size(); i++) {
        int index = m_bigRingIndices.at(i);
        if(index >= 0 && index < m_screenLights.size()){
            m_screenLights[index].intensity = intensity;
        }
    }
}

void SceneLights::setIntensityMappingLights(float intensity)
{
    for(int i=0; i<m_mappingIndices.size(); i++){
        int index = m_mappingIndices.at(i);
        if(index >= 0 && index < m_screenLights.size()){
            m_screenLights[index].intensity = intensity;
        }
    }
}


SceneLights::SceneLight SceneLights::loadLight(QString lightTag)
{
    SceneLight light;
    QJsonObject jsonObj = m_jsonObject.value(lightTag).toObject();

    QJsonArray jsonColor = jsonObj.value("color").toArray();
    for(int i=0; i<jsonColor.size(); i++){
        light.color[i] = jsonColor[i].toDouble(0);
    }
    QJsonArray jsonPosition = jsonObj.value("position").toArray();
    for(int i=0; i<jsonPosition.size(); i++){
        light.position[i] = jsonPosition[i].toDouble(0);
    }
    light.cutoff = jsonObj.value("cutoff").toDouble(0);
    light.angleAttenuation = jsonObj.value("angleAttenuation").toDouble(0);
    light.distanceAttenuation = jsonObj.value("distanceAttenuation").toDouble(0);

    return light;
}


void SceneLights::genSSBOBufferLights()
{
    glGenBuffers(1, &m_ssboLights);
}

void SceneLights::genSSBOBufferScreenLights()
{
    glGenBuffers(1, &m_ssboScreens);
}

void SceneLights::passLightsToShader(int bindingIndex)
{
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssboLights);
    glBufferData(GL_SHADER_STORAGE_BUFFER, m_lights.size() * sizeof(SceneLight), &m_lights[0], GL_STATIC_DRAW);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingIndex, m_ssboLights);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0); // unbind
}

void SceneLights::passScreenLightsToShader(int bindingIndex)
{
    if(!m_screenLights.isEmpty()){
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_ssboScreens);
        glBufferData(GL_SHADER_STORAGE_BUFFER, m_screenLights.size() * sizeof(SceneLight), &m_screenLights[0], GL_STATIC_DRAW);
        glBindBufferBase(GL_SHADER_STORAGE_BUFFER, bindingIndex, m_ssboScreens);
        glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0); // unbind
    }
}


void SceneLights::createLights(Object3D lightsObject)
{
    // Position of light here is in world coordinates before it is scaled by model matrix and translated by view matrix in shader (as model).
    // Therefore it is big coordinates (as model in obj file, before scaling)
    // Intensity can be more than 1.0, for multiplication

    SceneLight light1 = loadLight("mainLight1");
    light1.direction = glm::vec4(0, -1, 0, 0);
    m_lights.append(light1);

    SceneLight light2 = loadLight("mainLight2");
    light2.position = glm::vec4(0, lightsObject.minY()-1, 0, 1);
    light2.direction = glm::vec4(0, -1, 0, 0);
    m_lights.append(light2);
}

void SceneLights::createMainScreenLights(Object3D mainScreens)
{
    // Position of light here is in world coordinates before it is scaled by model matrix and translated by view matrix in shader (as model).
    // Therefore it is big coordinates (as model in obj file, before scaling)
    // Intensity can be more than 1.0, for multiplication
    float xHalfSize = mainScreens.xSize() / 2.0f;
    float yHalfSize = mainScreens.ySize() / 2.0f;
    float zHalfSize = mainScreens.zSize() / 2.0f;

    if(m_lod == LevelOfDetails::medium || m_lod == LevelOfDetails::high){
        // left
        SceneLight light = loadLight("mainScreenLight");
        light.position = glm::vec4(mainScreens.minX() - 1, mainScreens.minY() + yHalfSize, mainScreens.minZ() + zHalfSize, 1);
        light.direction = glm::vec4(-1, 0, 0, 0);
        m_mainScreenIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // right
        light.position = glm::vec4(mainScreens.maxX() + 1, mainScreens.minY() + yHalfSize, mainScreens.minZ() + zHalfSize, 1);
        light.direction = glm::vec4(1, 0, 0, 0);
        m_mainScreenIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // back
        light.position = glm::vec4(mainScreens.minX() + xHalfSize, mainScreens.minY() + yHalfSize, mainScreens.minZ() -1, 1);
        light.direction = glm::vec4(0, 0, -1, 0);
        m_mainScreenIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // front
        light.position = glm::vec4(mainScreens.minX() + xHalfSize, mainScreens.minY() + yHalfSize, mainScreens.maxZ() + 1, 1);
        light.direction = glm::vec4(0, 0, 1, 0);
        m_mainScreenIndices.append(m_screenLights.size());
        m_screenLights.append(light);
    }
}

void SceneLights::createTopRingLights(Object3D topRing)
{
    // Position of light here is in world coordinates before it is scaled by model matrix and translated by view matrix in shader (as model).
    // Therefore it is big coordinates (as model in obj file, before scaling)
    // Intensity can be more than 1.0, for multiplication
    float xHalfSize = topRing.xSize() / 2.0f;
    float yHalfSize = topRing.ySize() / 2.0f;
    float zHalfSize = topRing.zSize() / 2.0f;

    if(m_lod == LevelOfDetails::high){
        // left
        SceneLight light = loadLight("topRingLight");
        light.position = glm::vec4(topRing.minX() - 1, topRing.minY() + yHalfSize, topRing.minZ() + zHalfSize, 1);
        light.direction = glm::vec4(-1, 0, 0, 0);
        m_topRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // right
        light.position = glm::vec4(topRing.maxX() + 1, topRing.minY() + yHalfSize, topRing.minZ() + zHalfSize, 1);
        light.direction = glm::vec4(1, 0, 0, 0);
        m_topRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // back
        light.position = glm::vec4(topRing.minX() + xHalfSize, topRing.minY() + yHalfSize, topRing.minZ() -1, 1);
        light.direction = glm::vec4(0, 0, -1, 0);
        m_topRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // front
        light.position = glm::vec4(topRing.minX() + xHalfSize, topRing.minY() + yHalfSize, topRing.maxZ() + 1, 1);
        light.direction = glm::vec4(0, 0, 1, 0);
        m_topRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);
    }
}

void SceneLights::createBottomRingLights(Object3D bottomRing)
{
    // Position of light here is in world coordinates before it is scaled by model matrix and translated by view matrix in shader (as model).
    // Therefore it is big coordinates (as model in obj file, before scaling)
    // Intensity can be more than 1.0, for multiplication
    float xHalfSize = bottomRing.xSize() / 2.0f;
    float yHalfSize = bottomRing.ySize() / 2.0f;
    float zHalfSize = bottomRing.zSize() / 2.0f;

    if(m_lod == LevelOfDetails::high){
        // left
        SceneLight light = loadLight("bottomRingLight");
        light.position = glm::vec4(bottomRing.minX() - 1, bottomRing.minY() + yHalfSize, bottomRing.minZ() + zHalfSize, 1);
        light.direction = glm::vec4(-1, 0, 0, 0);
        m_bottomRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // right
        light.position = glm::vec4(bottomRing.maxX() + 1, bottomRing.minY() + yHalfSize, bottomRing.minZ() + zHalfSize, 1);
        light.direction = glm::vec4(1, 0, 0, 0);
        m_bottomRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // back
        light.position = glm::vec4(bottomRing.minX() + xHalfSize, bottomRing.minY() + yHalfSize, bottomRing.minZ() -1, 1);
        light.direction = glm::vec4(0, 0, -1, 0);
        m_bottomRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // front
        light.position = glm::vec4(bottomRing.minX() + xHalfSize, bottomRing.minY() + yHalfSize, bottomRing.maxZ() + 1, 1);
        light.direction = glm::vec4(0, 0, 1, 0);
        m_bottomRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);
    }
}

void SceneLights::createMantinelLights(Object3D mantinelScreen)
{
    // Position of light here is in world coordinates before it is scaled by model matrix and translated by view matrix in shader (as model).
    // Therefore it is big coordinates (as model in obj file, before scaling)
    // Intensity can be more than 1.0, for multiplication
    int numberOfLights = 0;
    if(m_lod == LevelOfDetails::high){
        numberOfLights = 20;
    }

    float xOffset = mantinelScreen.xSize() / (numberOfLights-1);
    float yHalfSize = mantinelScreen.ySize() / 2.0f;

    SceneLight light = loadLight("mantinelLight");
    light.direction = glm::vec4(0, 1, 10, 0);

    for(int i=0; i<numberOfLights; i++){
        light.position = glm::vec4(mantinelScreen.minX() + i*xOffset, mantinelScreen.minY() + yHalfSize, mantinelScreen.minZ(), 1);
        m_mantinelIndices.append(m_screenLights.size());
        m_screenLights.append(light);
    }
}

void SceneLights::createBigRingLights(Object3D bigRing)
{
    // Position of light here is in world coordinates before it is scaled by model matrix and translated by view matrix in shader (as model).
    // Therefore it is big coordinates (as model in obj file, before scaling)
    // Intensity can be more than 1.0, for multiplication
    float xHalfSize = bigRing.xSize() / 2.0f;
    float yHalfSize = bigRing.ySize() / 2.0f;
    float zHalfSize = bigRing.zSize() / 2.0f;

    SceneLight light = loadLight("bigRingLight");

    if(m_lod == LevelOfDetails::medium || m_lod == LevelOfDetails::high){
        // left
        light.position = glm::vec4(bigRing.minX() + 1, bigRing.minY() + yHalfSize, bigRing.minZ() + zHalfSize, 1);
        light.direction = glm::vec4(1, 0, 0, 0);
        m_bigRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // right
        light.position = glm::vec4(bigRing.maxX() - 1, bigRing.minY() + yHalfSize, bigRing.minZ() + zHalfSize, 1);
        light.direction = glm::vec4(-1, 0, 0, 0);
        m_bigRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // front
        light.position = glm::vec4(bigRing.minX() + xHalfSize, bigRing.minY() + yHalfSize, bigRing.minZ() + 1, 1);
        light.direction = glm::vec4(0, 0, 1, 0);
        m_bigRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // back
        light.position = glm::vec4(bigRing.minX() + xHalfSize, bigRing.minY() + yHalfSize, bigRing.maxZ() - 1, 1);
        light.direction = glm::vec4(0, 0, -1, 0);
        m_bigRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

    }
    if(m_lod == LevelOfDetails::high){

        // front left
        light.position = glm::vec4(bigRing.minX() + xHalfSize/5.2, bigRing.minY() + yHalfSize, bigRing.minZ() + zHalfSize/5.2, 1);
        light.direction = glm::vec4(1, 0, 1, 0);
        m_bigRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // front right
        light.position = glm::vec4(bigRing.maxX() - xHalfSize/5.2, bigRing.minY() + yHalfSize, bigRing.minZ() + zHalfSize/5.2, 1);
        light.direction = glm::vec4(-1, 0, 1, 0);
        m_bigRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // back left
        light.position = glm::vec4(bigRing.minX() + xHalfSize/5.2, bigRing.minY() + yHalfSize, bigRing.maxZ() - zHalfSize/5.2, 1);
        light.direction = glm::vec4(1, 0, -1, 0);
        m_bigRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);

        // back right
        light.position = glm::vec4(bigRing.maxX() - xHalfSize/5.2, bigRing.minY() + yHalfSize, bigRing.maxZ() - zHalfSize/5.2, 1);
        light.direction = glm::vec4(-1, 0, -1, 0);
        m_bigRingIndices.append(m_screenLights.size());
        m_screenLights.append(light);
    }
}

void SceneLights::createMappingLights(Object3D lightsObject)
{
    // Position of light here is in world coordinates before it is scaled by model matrix and translated by view matrix in shader (as model).
    // Therefore it is big coordinates (as model in obj file, before scaling)
    // Intensity can be more than 1.0, for multiplication

    SceneLight light = loadLight("mappingLight");
    light.position = glm::vec4(0, lightsObject.minY()-1, 0, 1);
    light.direction = glm::vec4(0, -1, 0, 0);
    m_mappingIndices.append(m_screenLights.size());
    m_screenLights.append(light);
}
