#include "positionselector.h"

PositionSelector::PositionSelector(QString pathToPicture, QWidget* parent) :
    QLabel(parent)
{
    bool successful = m_image.load(pathToPicture);
    if(successful){
        m_image = m_image.scaled(mapWidth, mapHeight, Qt::IgnoreAspectRatio);
        setFixedSize(mapWidth, mapHeight);
    } else {
        m_image = QImage(mapWidth, mapHeight, QImage::Format_RGBA8888);
        m_image.fill(Qt::transparent);
        setFixedSize(mapWidth, mapHeight);
    }
}

PositionSelector::~PositionSelector()
{
    savePoints();
    m_selectedPositions.clear();
    m_selectedPositions.squeeze();
}

void PositionSelector::switchToNextCamera()
{
    if(m_selectedPositions.isEmpty()){
        return;
    }
    int index = m_selectedPositions.indexOf(m_currentPosition) + 1;
    if(index >= m_selectedPositions.size()){
        index  = 0;
    }
    m_currentPosition = m_selectedPositions.at(index);
    emit positionChanged(m_currentPosition);
    update();
}


void PositionSelector::cameraPositionChanged()
{
    m_currentPosition = QPointF();
    update();
}


void PositionSelector::removePoint(QPointF position)
{
    if(positionOutOfStadium(position)){
        return;
    }
    int positionToDeselect = -1;

    for(int i=0; i<m_selectedPositions.size(); i++){
        if(pointsAreClose(position, m_selectedPositions.at(i))){
            positionToDeselect = i;
        }
    }
    if(positionToDeselect != -1){
        m_selectedPositions.remove(positionToDeselect);
    }
    if(pointsAreClose(position, m_currentPosition)){
        m_currentPosition = QPointF();
    }
}

void PositionSelector::selectPoint(QPointF position)
{
    if(positionOutOfStadium(position)){
        return;
    }
    int positionToSelect = -1;

    for(int i=0; i<m_selectedPositions.size(); i++){
        if(pointsAreClose(position, m_selectedPositions.at(i))){
            positionToSelect = i;
        }
    }
    if(positionToSelect != -1){
        m_currentPosition = m_selectedPositions.at(positionToSelect);
        emit positionChanged(m_currentPosition);
    } else {
        m_selectedPositions.append(position);
    }
}

bool PositionSelector::pointsAreClose(QPointF point1, QPointF point2)
{
    if((point1.x() < point2.x()+clickDistanceTolerance) &&
       (point1.x() > point2.x()-clickDistanceTolerance) &&
       (point1.y() < point2.y()+clickDistanceTolerance) &&
       (point1.y() > point2.y()-clickDistanceTolerance))
    {
        return true;
    }
    return false;
}

bool PositionSelector::positionOutOfStadium(QPointF position)
{
    if(m_image.pixelColor((int)position.x(), (int)position.y()).alpha() == 0){
        return true;
    }
    return false;
}

//QPointF PositionSelector::getRelativePoint(QPointF point)
//{
//    // map positions to interval [-1,1]
//    float x = (point.x() / (width() / 2)) - 1;
//    float y = (point.y() / (height() / 2)) - 1;
//    return QPointF(x,y);
//}

//QPointF PositionSelector::getLocalPoint(QPointF point)
//{
//    // map positions to widget coordinates
//    float x = ((point.x() + 1) * (width() / 2)) ;
//    float y = ((point.y() + 1) * (height() / 2));
//    return QPointF(x,y);
//}


void PositionSelector::savePoints()
{
    QFile file("camera_positions.json");
    if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        qDebug() << "[ERROR]\t PositionSelector::savePoints - unable save to file: " << file.fileName();
        return;
    }

    QJsonObject object;
    if(!m_selectedPositions.isEmpty()){
        QJsonArray array;
        for(int i=0; i<m_selectedPositions.size(); i++){
            QJsonObject point;
            point.insert("x", m_selectedPositions.at(i).x());
            point.insert("y", m_selectedPositions.at(i).y());
            array.append(QJsonValue(point));
        }
        object.insert("positions", array);
    }
    if(!m_currentPosition.isNull()){
        QJsonObject point;
        point.insert("x", m_currentPosition.x());
        point.insert("y", m_currentPosition.y());
        object.insert("currentPosition", point);
    }

    QJsonDocument document(object);
    file.write(document.toJson());
    file.close();
}

void PositionSelector::loadPoints()
{
    QFile file("camera_positions.json");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "[ERROR]\t PositionSelector::loadPoints - unable to open file: " << file.fileName();
        return;
    }

    QJsonDocument document =  QJsonDocument::fromJson(file.readAll());
    if(document.isNull()) {
        qDebug() << "[ERROR]\t PositionSelector::loadPoints - corrupted (invalid JSON) config file " << file.fileName();
        return;
    }
    file.close();

    QJsonObject object = document.object();
    QJsonArray array = object.value("positions").toArray();
    for(int i=0; i<array.size(); i++){
        QJsonObject point = array.at(i).toObject();
        QPointF pointF(point.value("x").toDouble(), point.value("y").toDouble());
        m_selectedPositions.append(pointF);
    }

    QJsonObject point = object.value("currentPosition").toObject();
    m_currentPosition = QPointF(point.value("x").toDouble(), point.value("y").toDouble());
    if(!m_currentPosition.isNull()){
        emit positionChanged(m_currentPosition);
    }
}


void PositionSelector::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::RightButton){
        removePoint(event->localPos());
    } else if(event->button() == Qt::LeftButton){
        selectPoint(event->localPos());
    }
    update();
}

void PositionSelector::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    QBrush brush(Qt::red, Qt::SolidPattern);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.setBrush(brush);

    QRectF target(0, 0, width(), height());
    QRectF source(0, 0, m_image.width(), m_image.height());
    painter.drawImage(target, m_image, source);

    for(int i=0; i<m_selectedPositions.size(); i++){
        painter.drawEllipse(m_selectedPositions.at(i), 5, 5);
    }

    if(!m_currentPosition.isNull()){
        brush.setColor(Qt::green);
        painter.setBrush(brush);
        painter.drawEllipse(m_currentPosition, 5, 5);
    }
}
