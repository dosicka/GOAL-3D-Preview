#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include <QImage>
#include <QObject>
#include <QTimer>
#include <mutex>

#include <vlc/libvlc.h>
#include <vlc/libvlc_media.h>
#include <vlc/libvlc_media_player.h>
#include <vlc/libvlc_events.h>

class MediaPlayer : public QObject
{
    Q_OBJECT

    enum MediaType
    {
        image,
        video,
        none
    };

    libvlc_instance_t *m_vlcInstance = nullptr;
    libvlc_media_player_t *m_mediaPlayer = nullptr;
    unsigned m_height = 0;
    unsigned m_width = 0;

    QImage m_image;

    std::mutex m_imageMutex;

    libvlc_event_manager_t *m_eventManager = nullptr;

    bool m_sendSignal;

    int m_FrameIDcounter;
    int m_repeat{0};
    int m_alreadyRepeated{0};
    QString m_path;

    QTimer imageTimer;

    int m_mediaID;

    MediaType currentMedia = {none};

public:

    MediaPlayer(QObject *parent = 0);
    ~MediaPlayer();

    void setVolume(int volume);
    bool sendSignal() const;
    void setSendSignal(bool sendSignal);
    void sendVideoFailed();

    void startImage();

    int mediaID() const;
    void setMediaID(int mediaID);

    int volume() const;

    bool wasPaused{false};

public slots:

    void play();
    void stop();
    void setMedia(QString path, int repeat = 0);

private slots:

    void videoEnded();

signals:

    void imageReady(QImage image,int frameID);
    void mediaPlayerFinished();

    void setElementVisible(bool);
    void videoLoaded(bool success, QString pathToVideoFile);
    void signalVideoEnded();
    void videoFailed(QString);

private:

    static void *lock(void *data, void **p_pixels);
    static void unlock(void *data, void *id, void *const *p_pixels);
    static unsigned video_format_cb(void **opaque, char *chroma,
                                    unsigned *width, unsigned *height,
                                    unsigned *pitches,
                                    unsigned *lines);
    static void eventCallback(const struct libvlc_event_t* event, void* userData);
};

#endif // MEDIAPLAYER_H
