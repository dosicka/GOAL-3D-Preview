#ifndef SHADERLOADER_H
#define SHADERLOADER_H

#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>
#include <glm/glm.hpp>
//#include "stdafx.h"
//#include <GL/glew.h>
//#include <GL/glut.h>

#include <cstring>
#include <iostream>
#include <string>
#include <fstream>

#include <QDir>
#include <QFile>
#include <QCoreApplication>
#include <QTextStream>
#include <QDebug>


class ShaderLoader {

public :

    static std::string textFileRead (const char * pathToFile) ;
    static void programerrors (const GLint program) ;
    static void shadererrors (const GLint shader) ;
    static GLuint initshaders (GLenum type, const char * filename) ;
    static GLuint initprogram (GLuint vertexshader, GLuint fragmentshader) ;
};


#endif // SHADERLOADER_H
