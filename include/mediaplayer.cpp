#include "mediaplayer.h"

#include <QDebug>

#include <vlc_plugin.h>
#include <vlc_fourcc.h>
#include <vlc_es.h>
#include <malloc.h>


MediaPlayer::MediaPlayer(QObject *parent) : QObject(parent)
{
    connect(this,SIGNAL(signalVideoEnded()),this,SLOT(videoEnded()));
    connect(&imageTimer,SIGNAL(timeout()),this,SLOT(videoEnded()));

    m_FrameIDcounter = 0;
    m_sendSignal = true;
    const char * const vlc_args[] = {
        "--verbose=0",
        "--quiet",
        "--no-osd"
    };

    m_vlcInstance = libvlc_new(sizeof(vlc_args) / sizeof(vlc_args[0]), vlc_args);

    if (!m_vlcInstance) {
        fprintf(stderr, "[FAIL]\t initialize vlc instance");
        return;
    }

    m_mediaPlayer = libvlc_media_player_new(m_vlcInstance);
    if(m_mediaPlayer==NULL) {
        fprintf(stderr, "[FAIL]\t initialize vlc player");
        return;
    }
    m_eventManager = libvlc_media_player_event_manager(m_mediaPlayer);

    libvlc_event_attach(m_eventManager, libvlc_MediaPlayerPaused , eventCallback, this);
    libvlc_event_attach(m_eventManager, libvlc_MediaPlayerEndReached , eventCallback, this);
    libvlc_event_attach(m_eventManager, libvlc_MediaPlayerPlaying, eventCallback, this);
    libvlc_event_attach(m_eventManager, libvlc_MediaPlayerMediaChanged, eventCallback, this);
    libvlc_event_attach(m_eventManager, libvlc_MediaPlayerEndReached, eventCallback, this);
    libvlc_event_attach(m_eventManager, libvlc_MediaPlayerStopped, eventCallback, this);
    libvlc_event_attach(m_eventManager, libvlc_MediaPlayerEncounteredError, eventCallback, this);

    libvlc_video_set_callbacks(m_mediaPlayer, lock, unlock, NULL, this);
    libvlc_video_set_format_callbacks(m_mediaPlayer, video_format_cb, NULL);
}

MediaPlayer::~MediaPlayer()
{
    libvlc_media_player_set_media(m_mediaPlayer, NULL);

    if(m_mediaPlayer){
        libvlc_media_player_release(m_mediaPlayer);
        m_mediaPlayer = nullptr;
    }

    if(m_vlcInstance){
        libvlc_release(m_vlcInstance);
        m_vlcInstance = nullptr;
    }
}

void MediaPlayer::play()
{
    libvlc_media_player_play(m_mediaPlayer);
}

void MediaPlayer::setVolume(int volume)
{
    libvlc_audio_set_volume(m_mediaPlayer, volume);
}

void MediaPlayer::stop()
{
    libvlc_media_player_stop(m_mediaPlayer);
}

void MediaPlayer::setMedia(QString path, int repeat)
{
    if(path.isNull() || path.isEmpty()){
        return;
    }

    libvlc_media_t *vlc_media = libvlc_media_new_path(m_vlcInstance, path.toStdString().c_str());
    if(vlc_media)
    {
        m_alreadyRepeated = 0;
        m_repeat = repeat;
        m_path = path;
        libvlc_media_player_set_media(m_mediaPlayer, vlc_media);
        currentMedia = video;
    }
}

void *MediaPlayer::lock(void *data, void **p_pixels)
{
    MediaPlayer* t = (MediaPlayer*)data;
    t->m_imageMutex.lock();
    t->m_image = QImage(t->m_width, t->m_height, QImage::Format_RGBA8888);
    *p_pixels = t->m_image.bits();
    return *p_pixels;
}

void MediaPlayer::unlock(void *data, void *id, void * const *p_pixels)
{
    Q_UNUSED(id);
    Q_UNUSED(p_pixels);

    MediaPlayer* t = (MediaPlayer*)data;
    emit t->imageReady(t->m_image,t->m_FrameIDcounter++);
    t->m_imageMutex.unlock();
}

unsigned MediaPlayer::video_format_cb(void **opaque, char *chroma, unsigned *width, unsigned *height, unsigned *pitches, unsigned *lines)
{
    MediaPlayer* t = (MediaPlayer*)*opaque;

    unsigned int videoWidth = 0;
    unsigned int videoHeight = 0;

    libvlc_video_get_size(t->m_mediaPlayer, 0, &videoWidth, &videoHeight);  //get real video size, not buffer size

    t->m_width  = videoWidth;
    t->m_height = videoHeight;
    *width = videoWidth;
    *height = videoHeight;

    strncpy(chroma,"RGBA",4);
    pitches[0] = (*width) * 4;
    pitches[1] = pitches[2] = 0;
    lines[0] = *height;
    lines[1] = lines[2] = 0;

    return 1u;
}

void MediaPlayer::eventCallback(const libvlc_event_t *event, void *userData)
{
    MediaPlayer* t = (MediaPlayer*)userData;
    switch (event->type)
    {
    case libvlc_MediaPlayerPlaying : {
        t->startImage();
        t->wasPaused = false;
        break;
    }
    case libvlc_MediaPlayerPaused : {
        t->wasPaused = true;
        break;
    }
    case libvlc_MediaPlayerStopped : {
        if(t->sendSignal()) {
            t->setSendSignal(false);
            emit t->videoLoaded(true, t->m_path);
        }
        t->wasPaused = false;
        QImage blackImg(t->m_width, t->m_height, QImage::Format_RGBA8888);
        blackImg.fill(Qt::black);
        emit t->imageReady(blackImg, -2);
        break;
    }
    case libvlc_MediaPlayerEndReached : {
        emit t->signalVideoEnded();
        break;
    }
    case libvlc_MediaPlayerEncounteredError : {
        emit t->setElementVisible(true);
        emit t->videoLoaded(false, t->m_path);
        t->wasPaused = false;
        break;
    }
    case libvlc_MediaPlayerMediaChanged:
        break;
    default:
        //qDebug() << "event not processed";
        break;
    }
}

int MediaPlayer::mediaID() const
{
    return m_mediaID;
}

void MediaPlayer::setMediaID(int mediaID)
{
    m_mediaID = mediaID;
}

int MediaPlayer::volume() const
{
    return libvlc_audio_get_volume(m_mediaPlayer);
}

bool MediaPlayer::sendSignal() const
{
    return m_sendSignal;
}

void MediaPlayer::setSendSignal(bool sendSignal)
{
    m_sendSignal = sendSignal;
}

void MediaPlayer::sendVideoFailed()
{
    emit videoFailed(m_path);
}

void MediaPlayer::startImage()
{
    if(currentMedia == image){
        if(!imageTimer.isActive())
            imageTimer.start();
    }
}

// repeat video if repeat number is set
void MediaPlayer::videoEnded()
{
    if(m_repeat > 0){
        m_alreadyRepeated++;

        if(m_alreadyRepeated == m_repeat){
            emit mediaPlayerFinished();
            return;
        }

        libvlc_media_t *vlc_media = libvlc_media_new_path(m_vlcInstance, m_path.toStdString().c_str());
        if(vlc_media)
        {
            libvlc_media_player_set_media(m_mediaPlayer, vlc_media);
            play();
        }
    } else if(m_repeat == 0) {
        libvlc_media_t *vlc_media = libvlc_media_new_path(m_vlcInstance, m_path.toStdString().c_str());
        if(vlc_media){
            libvlc_media_player_set_media(m_mediaPlayer, vlc_media);
            play();
        }
    } else {
        emit mediaPlayerFinished();
    }
}
