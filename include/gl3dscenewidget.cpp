#include "gl3dscenewidget.h"

GL3DSceneWidget::GL3DSceneWidget(QWidget *parent) :
    QOpenGLWidget(parent)
{
    // CAMERA
    // Coordinates for camera are assumed as after scaling by model matrix.
    // Because from these coordinates are calculated view and projection matrices
    // and it is not scaled by model matrix anymore. So must by smaller as already scaled.
    camera.setPosition(40, 10, 0);
    camera.setForwardVector(-1,0,0); // do not change
    camera.setUpVector(0,1,0);
    camera.setFovy(45.0f);
    camera.setNearClipPlane(0.1f);
    camera.setFarClipPlane(1000.0f);

    // MATERIALS
    Object3D::Material concreteMaterial;
    concreteMaterial.ambientCoef = glm::vec4(0.3, 0.3, 0.3, 1);
    concreteMaterial.diffuseCoef = glm::vec4(0.5, 0.5, 0.5, 1);
    concreteMaterial.specularCoef = glm::vec4(0.1, 0.1, 0.1, 1);
    concreteMaterial.shinesCoef = 10;
    concreteMaterial.alphaChanel = 1;

    Object3D::Material cubeMaterial;
    cubeMaterial.ambientCoef = glm::vec4(0.3, 0.3, 0.3, 1);
    cubeMaterial.diffuseCoef = glm::vec4(0.5, 0.5, 0.5, 1);
    cubeMaterial.specularCoef = glm::vec4(0.5, 0.5, 0.5, 1);
    cubeMaterial.shinesCoef = 10;
    cubeMaterial.alphaChanel = 1;

    Object3D::Material metalMaterial;
    metalMaterial.ambientCoef = glm::vec4(0.3, 0.3, 0.3, 1);
    metalMaterial.diffuseCoef = glm::vec4(0.5, 0.5, 0.5, 1);
    metalMaterial.specularCoef = glm::vec4(0.5, 0.5, 0.5, 1);
    metalMaterial.shinesCoef = 600;
    metalMaterial.alphaChanel = 1;

    Object3D::Material glassMaterial;
    glassMaterial.ambientCoef = glm::vec4(0.3, 0.3, 0.3, 1);
    glassMaterial.diffuseCoef = glm::vec4(0.5, 0.5, 0.5, 1);
    glassMaterial.specularCoef = glm::vec4(0.8, 0.8, 0.8, 1);
    glassMaterial.shinesCoef = 100;
    glassMaterial.alphaChanel = 0.3;

    Object3D::Material iceMaterial;
    iceMaterial.ambientCoef = glm::vec4(0.3, 0.3, 0.3, 1);
    iceMaterial.diffuseCoef = glm::vec4(0.9, 0.9, 0.9, 1);
    iceMaterial.specularCoef = glm::vec4(0.9, 0.9, 0.9, 1);
    iceMaterial.shinesCoef = 300;
    iceMaterial.alphaChanel = 1;

    Object3D::Material mantinelMaterial;
    mantinelMaterial.ambientCoef = glm::vec4(0.6, 0.6, 0.6, 1);
    mantinelMaterial.diffuseCoef = glm::vec4(0.8, 0.8, 0.8, 1);
    mantinelMaterial.specularCoef = glm::vec4(0.4, 0.4, 0.4, 1);
    mantinelMaterial.shinesCoef = 100;
    mantinelMaterial.alphaChanel = 1;

    // 3D OBJECTS
    stadium.setMaterial(concreteMaterial);
    tribune.setMaterial(concreteMaterial);
    roof.setMaterial(metalMaterial);
    cube.setMaterial(cubeMaterial);
    ice.setMaterial(iceMaterial);
    mantinelBottom.setMaterial(mantinelMaterial);
    mantinelGlass.setMaterial(glassMaterial);

    // ICE TEXTURE
    iceTexture.loadTexture(":/textures/resources/hockey_field.png");
}

GL3DSceneWidget::~GL3DSceneWidget()
{
    glDeleteFramebuffers(3, &frameBufferIDs[0]);
    glDeleteTextures(3, &depthTextures[0]);
    glDeleteProgram(renderProgram);
    glDeleteProgram(depthProgram);
    glDeleteProgram(testProgram);
}

void GL3DSceneWidget::setLevelOfDetails(SceneLights::LevelOfDetails levelOfDetails)
{
    lights.setLevelOfDetails(levelOfDetails);

    lights.createLights(lightsObject);
    lights.createMainScreenLights(mainScreens);
    lights.createTopRingLights(topRing);
    lights.createBottomRingLights(bottomRing);
    lights.createMantinelLights(mantinelScreen);
    lights.createBigRingLights(bigRing);
}

void GL3DSceneWidget::setPathTo3DObjects(QString pathToDir)
{
    QDir dir(pathToDir);
    QString path = dir.absolutePath();

    stadium.loadObject(path + "/stadium.obj");
    tribune.loadObject(path + "/tribune.obj");
    roof.loadObject(path + "/roof.obj");
    cube.loadObject(path + "/cube.obj");
    ice.loadObject(path + "/ice.obj");
    mantinelBottom.loadObject(path + "/mantinel_bottom.obj");
    mantinelGlass.loadObject(path + "/mantinel_glass.obj");
    mantinelScreen.loadObject(path + "/mantinel_screen.obj");
    mainScreens.loadObject(path + "/cube_4_screens.obj");
    bottomRing.loadObject(path + "/bottom_ring.obj");
    topRing.loadObject(path + "/top_ring.obj");
    bigRing.loadObject(path + "/big_ring.obj");
    lightsObject.loadObject(path + "/lights.obj");
}

void GL3DSceneWidget::setPathToLightConfig(QString pathToFile)
{
    lights.loadLightConfigFile(pathToFile);
}

void GL3DSceneWidget::initializeGL()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // toto povoluje různé funkce a presety, které jsou v OpenGL
    // Cull triangles which normal is not towards the camera - nemělo byt enabled, pak by nebyl face zezadu vidět
//    glEnable(GL_CULL_FACE);
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer or same distance to the camera than the former one - specify depth test method
    glDepthFunc(GL_LEQUAL); // LEQUAL is important for render multiple pass and blending
    // enable color blending from fragment shader to frame buffer
    glEnable(GL_BLEND);
    // antialiasing - possible different modes
//    glHint(GL_NICEST, GL_POLYGON_SMOOTH_HINT);
//    glHint(GL_NICEST, GL_LINE_SMOOTH_HINT);

    // SHADERS
    initializeShaders();

    // 3D OBJECTS
    stadium.bindGeometry();
    stadium.genMaterialBuffer();
    tribune.bindGeometry();
    tribune.genMaterialBuffer();
    roof.bindGeometry();
    roof.genMaterialBuffer();
    cube.bindGeometry();
    cube.genMaterialBuffer();
    ice.bindGeometry();
    ice.genMaterialBuffer();
    mantinelBottom.bindGeometry();
    mantinelBottom.genMaterialBuffer();
    mantinelGlass.bindGeometry();
    mantinelGlass.genMaterialBuffer();
    mantinelScreen.bindGeometry();
    mainScreens.bindGeometry();
    bottomRing.bindGeometry();
    topRing.bindGeometry();
    bigRing.bindGeometry();
    lightsObject.bindGeometry();

    // TEXTURES
    mainScreenTexture.bindToShader();
    topRingTexture.bindToShader();
    bottomRingTexture.bindToShader();
    mantinelScreenTexture.bindToShader();
    bigRingTexture.bindToShader();
    iceTexture.bindToShader();
    iceMappingTexture.bindToShader();

    // LIGHTS
    lights.genSSBOBufferLights();
    lights.genSSBOBufferScreenLights();

    // SHADOW MAPS
    // generete 3 frame buffers and textures
    glGenFramebuffers(3, &frameBufferIDs[0]);
    glGenTextures(3, &depthTextures[0]);
    createDepthBuffer(frameBufferIDs[0], depthTextures[0],
            shadowMapResolution, shadowMapResolution, DepthTextureType::shadowMap);
    createDepthBuffer(frameBufferIDs[1], depthTextures[1],
            shadowMapResolution, shadowMapResolution, DepthTextureType::shadowMap);
    createDepthBuffer(frameBufferIDs[2], depthTextures[2],
            depthMapWidth, depthMapHeight, DepthTextureType::heightMap);


/*
    // for testing only - shadow map texture view
    // The quad's FBO. Used only for visualizing the shadowmap.
    static const GLfloat g_quad_vertex_buffer_data[] = {
        -1.0f, -1.0f, 0.0f,
         1.0f, -1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
        -1.0f,  1.0f, 0.0f,
         1.0f, -1.0f, 0.0f,
         1.0f,  1.0f, 0.0f,
    };

    glGenBuffers(1, &quad_vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(g_quad_vertex_buffer_data), g_quad_vertex_buffer_data, GL_STATIC_DRAW);
*/

//    glFlush();    - na co?
}

void GL3DSceneWidget::paintGL()
{
    //////// RENDER TO FRAMEBUFFER //////////////////////

    if(!shadowComputed)
    {
        shadowComputed = true;

        // First of all, save the already bound framebuffer (framebuffer created by QOpenGLWidget)
        glGetIntegerv(GL_FRAMEBUFFER_BINDING, &qt_buffer);

        // view port here must be fixed to a size of texture
        // Render on the whole framebuffer, complete from the lower left corner to the upper right
        glViewport(0,0,shadowMapResolution,shadowMapResolution);
        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // cull front faces for rendering shadow map - not suitable for my model
        // when bias used this avoid to make peter panning on shadows, this parcially avoid shadow acne too
//        glEnable(GL_CULL_FACE);
//        glCullFace(GL_FRONT);

        // Use shader
        glUseProgram(depthProgram);

        // Coordinates for light here are assumed as after scaling by model matrix (as like camera).
        // Because from these coordinates are calculated view and projection matrices
        // and it is not scaled by model matrix anymore. So must by smaller as already scaled.
        updateCameraMatrices();
        updateLightMatrices(lights.lightAt(0), true);
        // Send our light transformation matrices to the currently bound shader
        glUniformMatrix4fv(lightProjectionMatLoc1, 1, GL_FALSE, &lightProjectionMatrix[0][0]);
        glUniformMatrix4fv(lightViewMatLoc1, 1, GL_FALSE, &lightViewMatrix[0][0]);
        glUniformMatrix4fv(modelMatrixLoc1, 1, GL_FALSE, &modelMatrix[0][0]);

    // 1. shadow map - neprůsvitné objekty
        // use created framebuffer to draw in it
        glBindFramebuffer(GL_FRAMEBUFFER, frameBufferIDs[0]);
        // Always check that our framebuffer is ok
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            return;

        stadium.drawVertices();
//        tribune.drawVertices();
        mantinelBottom.drawVertices();

   // 2. shadow map - sklo
        // use created framebuffer to draw in it
        glBindFramebuffer(GL_FRAMEBUFFER, frameBufferIDs[1]);
        // Always check that our framebuffer is ok
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            return;

        mantinelGlass.drawVertices();

    // 3. depth map - pro pozice kamery
        glViewport(0,0,depthMapWidth,depthMapHeight);
        // use created framebuffer to draw in it
        glBindFramebuffer(GL_FRAMEBUFFER, frameBufferIDs[2]);
        // Always check that our framebuffer is ok
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            return;

        // Clear the screen
//        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        SceneLights::SceneLight light;
        light.position = glm::vec4(0,stadium.maxY(),0,1);
        light.direction = glm::vec4(0,-1,0,0);

        updateLightMatrices(light, false);
        // Send our light transformation matrices to the currently bound shader
        glUniformMatrix4fv(lightProjectionMatLoc1, 1, GL_FALSE, &lightProjectionMatrix[0][0]);
        glUniformMatrix4fv(lightViewMatLoc1, 1, GL_FALSE, &lightViewMatrix[0][0]);
        glUniformMatrix4fv(modelMatrixLoc1, 1, GL_FALSE, &modelMatrix[0][0]);

        stadium.drawVertices();
        tribune.drawVertices();
//        ice.drawVertices();

        GLubyte* depthTexture = new GLubyte[4*depthMapWidth*depthMapHeight];
        glBindTexture(GL_TEXTURE_2D, depthTextures[2]);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, depthTexture);
        memcpy(&depthArray, depthTexture, 4*depthMapWidth*depthMapHeight);

//        QByteArray a;
//        for(int j=0; j<depthMapHeight; j++){
//            for(int i=0; i<depthMapWidth; i++){
//                a.append(QString("%1 ").arg(depthArray[j][i]));
//            }
//            a.append("\n");
//        }

//        QDir currentDir(QCoreApplication::applicationDirPath());
//        QFile file(currentDir.absoluteFilePath("array.txt"));
//        if(!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
//            qDebug() << "[ERROR]\t FullStringSettings::saveStringSettings - unable save to file: " << file.fileName();
//        }
//        file.write(a, a.size());
//        file.close();

        // restore frame buffer created by Qt at beginning
//        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, qt_buffer);
    }


    ////////// RENDER TO SCREEN //////////////////////

    // view port for screen rendering must be scaled to size of this widget
    glViewport(0, 0, this->width(), this->height());
    // Clear the screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    // Use our shader
    glUseProgram(renderProgram);

    // set blending to use only color from fragment shader and not that from frame buffer (old one)
    glBlendFunc(GL_ONE,GL_ZERO);

    // CAMERA MATRICES
    updateCameraMatrices();
    // Send uniform data to the currently bound shader - every frame
    glUniformMatrix4fv(cameraViewMatLoc, 1, GL_FALSE, &cameraViewMatrix[0][0]);
    glUniformMatrix4fv(cameraProjectionMatLoc, 1, GL_FALSE, &cameraProjectionMatrix[0][0]);
    glUniformMatrix3fv(normalMatrixLoc, 1, GL_FALSE, &normalMatrix[0][0]);
    glUniformMatrix4fv(modelMatrixLoc2, 1, GL_FALSE, &modelMatrix[0][0]);

    // LIGHT MATRICES
    updateLightMatrices(lights.lightAt(0), true);
    glUniformMatrix4fv(lightProjectionMatLoc2, 1, GL_FALSE, &lightProjectionMatrix[0][0]);
    glUniformMatrix4fv(lightViewMatLoc2, 1, GL_FALSE, &lightViewMatrix[0][0]);

    // LIGHTS
    lights.passScreenLightsToShader(3);
    lights.passLightsToShader(2);

    // Bind shadow map texture in Texture Unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, depthTextures[0]);
    glUniform1i(shadowMapOpaqueLoc, 0);

    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, depthTextures[1]);
    glUniform1i(shadowMapGlassLoc, 1);

    // OBJECTS
    renderScreens();
    renderLights();
    renderIceField();
    renderStadium();

    // enable alfa blending for glass
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    renderMantinelGlass();


    ////////// RENDER SHADOWMAP TEXTURE ////////////////
/*
    for(int i=2; i<3; i++)
    {
        // Render only on a corner of the window (or we won't see the real rendering...)
//        glViewport(0+i*200,0,200,200);
        glViewport(0,0,600,400);
        // do not use here, would erase previous rendering (scene)
    //    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Use our shader
        glUseProgram(testProgram);

        // Bind shadow map texture in Texture Unit 0
        glActiveTexture(GL_TEXTURE1 + i);
        glBindTexture(GL_TEXTURE_2D, depthTextures[i]);
        // Set our "renderedTexture" sampler to use Texture Unit 0
        glUniform1i(shadowMapLoc2, 1+i);

        glBlendFunc(GL_ONE,GL_ZERO);
        // 1rst attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, quad_vertexbuffer);
        glVertexAttribPointer(
            0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
            3,                  // size
            GL_FLOAT,           // type
            GL_FALSE,           // normalized?
            0,                  // stride
            (void*)0            // array buffer offset
        );
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glDisableVertexAttribArray(0);
    }
*/
}


void GL3DSceneWidget::resizeGL(int, int)
{
    //    glViewport(0, 0, w, h);
}


void GL3DSceneWidget::renderScreens()
{
    glUniform1i(renderStyleLoc, 1);

    glUniform1f(screenBrightnessLoc, lights.intensityMainScreen());
    mainScreenTexture.passToShader(screenTextureLoc, GL_TEXTURE2);
    mainScreens.draw();

    glUniform1f(screenBrightnessLoc, lights.intensityTopRing());
    topRingTexture.passToShader(screenTextureLoc, GL_TEXTURE2);
    topRing.draw();

    glUniform1f(screenBrightnessLoc, lights.intensityBottomRing());
    bottomRingTexture.passToShader(screenTextureLoc, GL_TEXTURE2);
    bottomRing.draw();

    glUniform1f(screenBrightnessLoc, lights.intensityMantinelScreen());
    mantinelScreenTexture.passToShader(screenTextureLoc, GL_TEXTURE2);
    mantinelScreen.draw();

    glUniform1f(screenBrightnessLoc, lights.intensityBigRing());
    bigRingTexture.passToShader(screenTextureLoc, GL_TEXTURE2);
    bigRing.draw();
}

void GL3DSceneWidget::renderLights()
{
    glUniform1i(renderStyleLoc, 2);
    lightsObject.draw();
}

void GL3DSceneWidget::renderMantinelGlass()
{
    glUniform1i(renderStyleLoc, 3);
    mantinelGlass.passMaterialToShader(1);
    mantinelGlass.draw();
}

void GL3DSceneWidget::renderIceField()
{
    glUniform1i(renderStyleLoc, 4);
    glUniform1f(screenBrightnessLoc, lights.intensityMapping());
    iceTexture.passToShader(objectTextureLoc, GL_TEXTURE3);
    iceMappingTexture.passToShader(screenTextureLoc, GL_TEXTURE2);
    ice.passMaterialToShader(1);
    ice.draw();
}

void GL3DSceneWidget::renderStadium()
{
    glUniform1i(renderStyleLoc, 3);
    stadium.passMaterialToShader(1);
    stadium.draw();
    tribune.passMaterialToShader(1);
    tribune.draw();
    roof.passMaterialToShader(1);
    roof.draw();
    cube.passMaterialToShader(1);
    cube.draw();
    mantinelBottom.passMaterialToShader(1);
    mantinelBottom.draw();
}


void GL3DSceneWidget::initializeShaders()
{
    // initialize shaders and create shader program
    GLuint depthVertex = ShaderLoader::initshaders(GL_VERTEX_SHADER, ":/shaders/include/shaders/depth_shader.vert");
    GLuint depthFragment = ShaderLoader::initshaders(GL_FRAGMENT_SHADER, ":/shaders/include/shaders/depth_shader.frag");
    depthProgram = ShaderLoader::initprogram(depthVertex, depthFragment);

    GLuint renderVertex = ShaderLoader::initshaders(GL_VERTEX_SHADER, ":/shaders/include/shaders/render_shader.vert") ;
    GLuint renderFragment = ShaderLoader::initshaders(GL_FRAGMENT_SHADER, ":/shaders/include/shaders/render_shader.frag") ;
    renderProgram = ShaderLoader::initprogram(renderVertex, renderFragment);

    GLuint testVertex = ShaderLoader::initshaders(GL_VERTEX_SHADER, ":/shaders/include/shaders/test_shader.vert");
    GLuint testFragment = ShaderLoader::initshaders(GL_FRAGMENT_SHADER, ":/shaders/include/shaders/test_shader.frag");
    testProgram = ShaderLoader::initprogram(testVertex, testFragment);

    // get uniform locations from shaders to load data
    lightProjectionMatLoc1 = glGetUniformLocation(depthProgram, "light_projection_matrix");
    lightViewMatLoc1 = glGetUniformLocation(depthProgram, "light_view_matrix");
    modelMatrixLoc1 = glGetUniformLocation(depthProgram, "model_matrix");

    modelMatrixLoc2 = glGetUniformLocation(renderProgram, "model_matrix");
    cameraProjectionMatLoc = glGetUniformLocation(renderProgram, "camera_projection_matrix");
    cameraViewMatLoc = glGetUniformLocation(renderProgram, "camera_view_matrix");
    normalMatrixLoc = glGetUniformLocation(renderProgram, "normal_matrix");
    lightProjectionMatLoc2 = glGetUniformLocation(renderProgram, "light_projection_matrix");
    lightViewMatLoc2 = glGetUniformLocation(renderProgram, "light_view_matrix");
    renderStyleLoc = glGetUniformLocation(renderProgram, "render_style");
    screenBrightnessLoc = glGetUniformLocation(renderProgram, "screen_brightness");
    objectTextureLoc = glGetUniformLocation(renderProgram, "object_texture");
    screenTextureLoc = glGetUniformLocation(renderProgram, "screen_texture");
    shadowMapOpaqueLoc = glGetUniformLocation(renderProgram, "shadow_map_opaque");
    shadowMapGlassLoc = glGetUniformLocation(renderProgram, "shadow_map_glass");

    shadowMapLoc2 = glGetUniformLocation(testProgram, "shadow_map");
}

void GL3DSceneWidget::updateCameraMatrices()
{
    // Generates standard 4x4 matrix - projection of space to screen
    cameraProjectionMatrix = glm::perspective(
                camera.fieldOfView(), // The vertical Field of View, in radians: the amount of "zoom". Think "camera lens". Usually between 90° (extra wide) and 30° (quite zoomed in)
                (float)this->width()/this->height(), // Aspect Ratio. Depends on the size of your window.
                camera.nearClipPlane(), // Near clipping plane. Keep as little as possible, or you'll get precision issues.
                camera.farClipPlane() // Far clipping plane. Keep as big as possible.
    );
    // for an ortho camera :
//    cameraProjectionMatrix = glm::ortho(-100, 100, -100, 100, 1, 10000); // In world coordinates

    // matrix gives position, direction and orientation of camera in space
    cameraViewMatrix = glm::lookAt(
                camera.position(), // cameraPosition - position of your camera, in world space
//                camera.forward(), // cameraTarget - where you want to look at, in world space, can be point like here
                camera.eyeForwardVector(),  // cameraTarget can be also vector like here,
                camera.up() // upVector - normal glm::vec3(0,1,0), but (0,-1,0) would make you looking upside-down
    );

    // Model matrix : an identity matrix (model will be at the origin)
//    glm::mat4 model_matrix = glm::mat4(1.0f);
    // použiji scale matrix, podle toho jak je model velkej ho scalenu, aby byl každej stejně
    modelMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(stadium.scaleCoeficient()));

//    glm::mat3 normal_matrix = glm::transpose(glm::inverse(glm::mat3(view_matrix)));
//    glm::mat3 normal_matrix = glm::transpose(glm::inverse(glm::mat3(model_matrix)));
    normalMatrix = glm::transpose(glm::inverse(glm::mat3(cameraViewMatrix*modelMatrix)));
}

void GL3DSceneWidget::updateLightMatrices(SceneLights::SceneLight light, bool perspective)
{
    // position here must be in model coordinate system, bcs matrices are not scaled my model_matrix in shader anymore
    glm::vec3 lightPosition = glm::vec3(modelMatrix * light.position);

    glm::vec3 upVector = glm::vec3(1,0,0);
    glm::vec3 direction = glm::vec3(light.direction);
    if(direction != upVector){
        upVector = glm::cross(direction, upVector);
    } else {
        upVector = glm::vec3(0,1,0);
    }

    // Compute the MVP matrix from the light's point of view
    if(perspective){
        // celkem dobrý fovy 60, abych mohl použit PCF v shaderu. Fovy se chová divně při různých hodnotách - nezvětšuje se lineárně, není to nějak divně interpolovaný mezi 0 - 1 nebo něco?
        lightProjectionMatrix = glm::perspective<float>(2.0f, 1.0f, 1.0f, 1000.0f); // perspective (spot light) - no bigger fovy than 180
    } else {
        lightProjectionMatrix = glm::ortho<float>(-(tribune.xSize()*stadium.scaleCoeficient())/2, (tribune.xSize()/2*stadium.scaleCoeficient()),
                                                  -(tribune.zSize()*stadium.scaleCoeficient())/2, (tribune.zSize()/2*stadium.scaleCoeficient()),
                                                  0.1f, stadium.ySize()*stadium.scaleCoeficient()-1); // orthogonal - (directional light)
    }

    lightViewMatrix = glm::lookAt(lightPosition, // if position is too low -> no shadow, bcs fovy does not cover mantinels
                                  glm::vec3(light.direction),
                                  upVector);
}

void GL3DSceneWidget::createDepthBuffer(GLuint &frameBufferID, GLuint &depthTextureID, int textureWidth, int textureHeight, DepthTextureType textureType)
{
    // Depth texture. Slower than a depth buffer, but you can sample it later in your shader
    glBindTexture(GL_TEXTURE_2D, depthTextureID);

    // The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
    glBindFramebuffer(GL_FRAMEBUFFER, frameBufferID);

    switch(textureType){
    case DepthTextureType::shadowMap :
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, textureWidth, textureHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        // set parameters of texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        // fragments out of shadow map texture -> no shadow (depends on values in array) | use GL_REPEAT when shadow out of shadow map texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        float borderColor[] = {1.0f, 0.0f, 0.0f, 1.0f};
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthTextureID, 0);
        glDrawBuffer(GL_NONE);
        break;
    }
    case DepthTextureType::heightMap :
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, textureWidth, textureHeight, 0, GL_RED, GL_FLOAT, NULL);

        // set parameters of texture
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

        // for color output - must be layout out in shader
        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, depthTextureID, 0);
        // Set the list of draw buffers.
        GLenum DrawBuffers[] = {GL_COLOR_ATTACHMENT0};
        glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers
        break;
    }
    }
}


void GL3DSceneWidget::mouseMoveEvent(QMouseEvent *event)
{
    qreal dx = (event->windowPos().x() - oldMouseX) / this->width();
    qreal dy = (event->windowPos().y() - oldMouseY) / this->height();
    oldMouseX = event->windowPos().x();
    oldMouseY = event->windowPos().y();

    if(event->buttons() == Qt::MiddleButton){
        moveCamera(dx, dy);
    } else if(event->buttons() == Qt::LeftButton){
        rotateCamera(dx, dy);
    } else if(event->buttons() == Qt::RightButton){
        translateCamera(dx, dy);
    }
}

void GL3DSceneWidget::mousePressEvent(QMouseEvent *event)
{
    oldMouseX = event->windowPos().x();
    oldMouseY = event->windowPos().y();
}

void GL3DSceneWidget::wheelEvent(QWheelEvent *event)
{
    zoomCamera(static_cast<float>(event->angleDelta().y()));
}


bool GL3DSceneWidget::isCollision(glm::vec3 position)
{
    // check X and Z coordinates
    float maxX = tribune.maxX()*stadium.scaleCoeficient();
    float maxZ = tribune.maxZ()*stadium.scaleCoeficient();

    if(position.x > maxX-2 || position.x < -maxX+2 || position.z > maxZ-2 || position.z < -maxZ+2){
        return true;
    }
    // stadium approximated by elipse
    if(pow(position.x, 2)/pow(maxX, 2) + pow(position.z, 2)/pow(maxZ, 2) > 1){
        return true;
    }

    // check Y coordinate
    float x = ((position.x / (tribune.maxX()*stadium.scaleCoeficient()) +1) / 2) * depthMapWidth;
    float z = ((position.z / (tribune.maxZ()*stadium.scaleCoeficient()) +1) / 2) * depthMapHeight;

    if(position.y > roof.minY()*stadium.scaleCoeficient()){
        return true;
    } else if(position.y < ice.maxY()*stadium.scaleCoeficient()+0.5){
        return true;
    }
    float y = depthArray[static_cast<int>(z)][static_cast<int>(x)];
    if(position.y < y+1){
        return true;
    }

    return false;
}

void GL3DSceneWidget::moveCamera(float distanceX, float distanceY)
{
    glm::vec3 pos = camera.positionAfterMove(distanceX, distanceY);
    if(!isCollision(pos)){
        camera.setPosition(pos.x, pos.y, pos.z);
        camera.updateVectorsAfterMove(distanceX, distanceY);
        emit cameraPositionChanged();
        update();
    }
}

void GL3DSceneWidget::zoomCamera(float zoomNumber)
{
    glm::vec3 pos = camera.positionAfterZoom(zoomNumber);
    if(!isCollision(pos)){
        camera.setPosition(pos.x, pos.y, pos.z);
        emit cameraPositionChanged();
        update();
    }
}

void GL3DSceneWidget::translateCamera(float distanceX, float distanceY)
{
    glm::vec3 pos = camera.positionAfterTranslate(distanceX, distanceY);
    if(!isCollision(pos)){
        camera.setPosition(pos.x, pos.y, pos.z);
        emit cameraPositionChanged();
        update();
    }
}

void GL3DSceneWidget::rotateCamera(float distanceX, float distanceY)
{
    camera.rotateCamera(distanceX, distanceY);
    update();
}

void GL3DSceneWidget::setCameraPosition(QPointF position)
{
    // map positions to interval [-1,1]
    float xr = (position.x() / (depthMapWidth / 2)) - 1;
    float yr = (position.y() / (depthMapHeight / 2)) - 1;

    // maximum X and Z size of stadium (tribune)
    float maxX = tribune.maxX()*stadium.scaleCoeficient();
    float maxZ = tribune.maxZ()*stadium.scaleCoeficient();

    // x, y, z positions in world coordinates
    float x = maxX * xr;
    float z = maxZ * yr;
    float y = depthArray[static_cast<int>(position.y())][static_cast<int>(position.x())]+4;

    if(maxX != 0 && maxZ != 0){
        // check X and Z coordinates and correct if too close to wall (move bit to center) - stadium approximated by elipse
        while(((pow(x, 2)/pow(maxX, 2) + pow(z, 2)/pow(maxZ, 2)) > 1) ||
              x > (maxX-2) || x < (-maxX+2) || z > (maxZ-2) || z < (-maxZ+2))
        {
            if(x > maxX/2){
                x -= 0.5;
            } else if(x < -maxX/2){
                x += 0.5;
            }
            if(z > maxZ/2){
                z -= 0.5;
            } else if(z < -maxZ/2){
                z += 0.5;
            }
        }
    }

    // corect problems with wrong depth map configuration - if y too small set bigger
    if((qAbs(z) > 15 || qAbs(x) > 28) && y < 5){
        y = 17;
    }

    camera.setPosition(x, y, z);
    if(x == 0 && y == 0){
        camera.setForwardVector(0, 0, -1);
    } else {
        camera.setForwardVector(-x,0,-z);
    }
    camera.setUpVector(0,1,0);
    update();
}


void GL3DSceneWidget::setFrameToMainScreen(QImage frame, int frameID)
{
    mainScreenTexture.setVideoFrame(frame, frameID);
    update();
}

void GL3DSceneWidget::setFrameToTopRing(QImage frame, int frameID)
{
    topRingTexture.setVideoFrame(frame, frameID);
    update();
}

void GL3DSceneWidget::setFrameToBottomRing(QImage frame, int frameID)
{
    bottomRingTexture.setVideoFrame(frame, frameID);
    update();
}

void GL3DSceneWidget::setFrameToMantinel(QImage frame, int frameID)
{
    iceTexture.blendMantinelTexture(frame);
    mantinelScreenTexture.setVideoFrame(frame, frameID);
    update();
}

void GL3DSceneWidget::setFrameToBigRing(QImage frame, int frameID)
{
    bigRingTexture.setVideoFrame(frame, frameID);
    update();
}

void GL3DSceneWidget::setFrameToMapping(QImage frame, int frameID)
{
    iceMappingTexture.setVideoFrame(frame, frameID);
    update();
}


void GL3DSceneWidget::setIntensityLight(float intensity)
{
    lights.setIntensityLights(intensity);
    update();
}

void GL3DSceneWidget::setIntensityMainScreen(float intensity)
{
    lights.setIntensityMainScreen(intensity);
    update();
}

void GL3DSceneWidget::setIntensityTopRing(float intensity)
{
    lights.setIntensityTopRing(intensity);
    update();
}

void GL3DSceneWidget::setIntensityBottomRing(float intensity)
{
    lights.setIntensityBottomRing(intensity);
    update();
}

void GL3DSceneWidget::setIntensityMantinel(float intensity)
{
    lights.setIntensityMantinelScreen(intensity);
    iceTexture.setMantinelIntensity(intensity);
    update();
}

void GL3DSceneWidget::setIntensityBigRing(float intensity)
{
    lights.setIntensityBigRing(intensity);
    update();
}

void GL3DSceneWidget::setIntensityMapping(float intensity)
{
    lights.setIntensityMapping(intensity);
    update();
}
