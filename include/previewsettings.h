#ifndef PREVIEWSETTINGS_H
#define PREVIEWSETTINGS_H

#include <QDialog>
#include <QFileDialog>
#include <QSettings>

#include "include/entities/scenelights.h"

namespace Ui {
class PreviewSettings;
}

class PreviewSettings : public QDialog
{
    Q_OBJECT

    Ui::PreviewSettings *ui;

    // když je intenzita nastavena na tuto hodnotu, výstup na obrazovce = vstupním hodnotám. Pokud je intenzita větší, obrazovka je přesvětlená
    const float screenLightsCoef = 100.0f;
    const float mainLightsCoef = 100.0f;

    QString m_sourcePathMainScreen;
    QString m_sourcePathTopRing;
    QString m_sourcePathBottomRing;
    QString m_sourcePathBigRing;
    QString m_sourcePathMantinel;
    QString m_sourcePathMapping;

    float m_intensityLight;
    float m_intensityMainScreen;
    float m_intensityTopRing;
    float m_intensityBottomRing;
    float m_intensityMantinel;
    float m_intensityBigRing;
    float m_intensityMapping;

    bool m_playMainScreen;
    bool m_playTopRing;
    bool m_playBottomRing;
    bool m_playBigRing;
    bool m_playMantinel;
    bool m_playMapping;

    // used after application restart
    SceneLights::LevelOfDetails m_lod;
    QString m_pathTo3DObjects;
    QString m_pathToLightConfig;

public:

    explicit PreviewSettings(QWidget *parent = 0);
    ~PreviewSettings();

    void init();

    QString pathToContentMainScreen();
    QString pathToContentTopRing();
    QString pathToContentBottomRing();
    QString pathToContentBigRing();
    QString pathToContentMantinel();

    float intensityLigth();
    float intensityMainScreen();
    float intensityTopRing();
    float intensityBottomRing();
    float intensityMantinel();
    float intensityBigRing();

    SceneLights::LevelOfDetails levelOfDetails();
    QString pathTo3DObjects();
    QString pathToLightConfig();

signals:

    void contentMainScreenChanged(QString pathToContent);
    void contentTopRingChanged(QString pathToContent);
    void contentBottomRingChanged(QString pathToContent);
    void contentBigRingChanged(QString pathToContent);
    void contentMantinelChanged(QString pathToContent);
    void contentMappingChanged(QString pathToContent);

    void intensityLightChanged(float intensity);
    void intensityMainScreenChanged(float intensity);
    void intensityTopRingChanged(float intensity);
    void intensityBottomRingChanged(float intensity);
    void intensityMantinelChanged(float intensity);
    void intensityBigRingChanged(float intensity);
    void intensityMappingChanged(float intensity);

    void playMainScreen();
    void playTopRing();
    void playBottomRing();
    void playMantinel();
    void playBigRing();
    void playMapping();

    void stopMainScreen();
    void stopTopRing();
    void stopBottomRing();
    void stopMantinel();
    void stopBigRing();
    void stopMapping();

private slots:

    void saveSettings();
    void loadSettings();

    void on_contentMainScreen_button_clicked();
    void on_contentTopRing_button_clicked();
    void on_contentBottomRing_button_clicked();
    void on_contentMantinel_button_clicked();
    void on_contentBigRing_button_clicked();
    void on_contentMapping_button_clicked();

    void on_light_spinBox_valueChanged(int arg1);
    void on_mainScreen_spinBox_valueChanged(int arg1);
    void on_topRing_spinBox_valueChanged(int arg1);
    void on_bottomRing_spinBox_valueChanged(int arg1);
    void on_mantinel_spinBox_valueChanged(int arg1);
    void on_bigRing_spinBox_valueChanged(int arg1);

    void on_playMainScreen_button_clicked();
    void on_playTopRing_button_clicked();
    void on_playBottomRing_button_clicked();
    void on_playMantinel_button_clicked();
    void on_playBigRing_button_clicked();
    void on_playMapping_button_clicked();

    void on_stopMainScreen_button_clicked();
    void on_stopTopRing_button_clicked();
    void on_stopBottomRing_button_clicked();
    void on_stopMantinel_button_clicked();
    void on_stopBigRing_button_clicked();
    void on_stopMapping_button_clicked();

    void on_lod_comboBox_activated(int index);
    void on_pathToObj_pushButton_clicked();
    void on_pathToLights_pushButton_clicked();
    void on_mapping_spinBox_valueChanged(int arg1);
};

#endif // PREVIEWSETTINGS_H
