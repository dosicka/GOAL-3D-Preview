#ifndef GL3DSCENEWIDGET_H
#define GL3DSCENEWIDGET_H

#define GL_GLEXT_PROTOTYPES
//#include <GL/glew.h>  // nevim esi musim includovat i glew.h
#include <GL/gl.h>
#include <GL/glext.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <math.h>

#include <QObject>
#include <QOpenGLWidget>
#include <QVector>
#include <QVector3D>
#include <QVector4D>
#include <QMouseEvent>
#include <QDebug>
#include <QApplication>
#include <QSettings>

#include "include/shaderloader.h"
#include "include/entities/object3d.h"
#include "include/entities/texture.h"
#include "include/entities/scenecamera.h"
#include "include/entities/scenelights.h"


class GL3DSceneWidget : public QOpenGLWidget
{
    Q_OBJECT

    enum DepthTextureType {
        shadowMap,
        heightMap
    };

    SceneCamera camera;
    SceneLights lights;
// objects
    Object3D stadium;
    Object3D tribune;
    Object3D roof;
    Object3D cube;
    Object3D ice;
    Object3D mantinelBottom;
    Object3D mantinelGlass;
    Object3D mantinelScreen;
    Object3D mainScreens;
    Object3D bottomRing;
    Object3D topRing;
    Object3D bigRing;
    Object3D lightsObject;
// shaderPrograms
    GLuint renderProgram = 0;
    GLuint depthProgram = 0;
    GLuint testProgram = 0;
// matrices
    // camera
    glm::mat4 modelMatrix = glm::mat4(0);
    glm::mat4 cameraProjectionMatrix = glm::mat4(0);
    glm::mat4 cameraViewMatrix = glm::mat4(0);
    glm::mat3 normalMatrix = glm::mat3(0);
    // light
    glm::mat4 lightProjectionMatrix = glm::mat4(0);
    glm::mat4 lightViewMatrix = glm::mat4(0);
// textures
    Texture mainScreenTexture;
    Texture topRingTexture;
    Texture bottomRingTexture;
    Texture mantinelScreenTexture;
    Texture bigRingTexture;
    Texture iceTexture;
    Texture iceMappingTexture;
// uniform locations
    // depth program
    GLuint lightProjectionMatLoc1 = 0;
    GLuint lightViewMatLoc1 = 0;
    GLuint modelMatrixLoc1 = 0;
    // render program
    GLuint modelMatrixLoc2 = 0;
    GLuint cameraProjectionMatLoc = 0;
    GLuint cameraViewMatLoc = 0;
    GLuint normalMatrixLoc = 0;
    GLuint lightProjectionMatLoc2 = 0;
    GLuint lightViewMatLoc2 = 0;
    GLuint renderStyleLoc = 0;
    GLuint screenBrightnessLoc = 0;
    GLuint screenTextureLoc = 0;
    GLuint objectTextureLoc = 0;
    GLuint shadowMapOpaqueLoc = 0;
    GLuint shadowMapGlassLoc = 0;
    // test program
    GLuint shadowMapLoc2 = 0;
 // shadows
    const int shadowMapResolution = 4096;
    bool shadowComputed = false;
    GLuint frameBufferIDs[3]; // 3 buffery tak jako textury
    GLuint depthTextures[3]; // 3 shadow mapy (1. - neprůsvitné objekty, 2. - sklo, 3. - hight map zhora pro pozice kamery)
    GLuint quad_vertexbuffer = 0;
// hight map for position selector - must have same size as scheme in positionselector
    const int depthMapWidth = 600;
    const int depthMapHeight = 400;
    float depthArray[400][600];
// other
    qreal oldMouseX = 0;
    qreal oldMouseY = 0;
    GLint qt_buffer;


public:

    GL3DSceneWidget(QWidget *parent = 0);
    ~GL3DSceneWidget();

    void setLevelOfDetails(SceneLights::LevelOfDetails levelOfDetails);
    void setPathTo3DObjects(QString pathToDir);
    void setPathToLightConfig(QString pathToFile);

    void initializeGL();
    void paintGL();
    void resizeGL(int, int);

    void moveCamera(float distanceX, float distanceY);
    void translateCamera(float distanceX, float distanceY);
    void rotateCamera(float distanceX, float distanceY);
    void zoomCamera(float zoomNumber);

public slots:

    void setCameraPosition(QPointF position);

    void setFrameToMainScreen(QImage frame, int frameID);
    void setFrameToTopRing(QImage frame, int frameID);
    void setFrameToBottomRing(QImage frame, int frameID);
    void setFrameToMantinel(QImage frame, int frameID);
    void setFrameToBigRing(QImage frame, int frameID);
    void setFrameToMapping(QImage frame, int frameID);

    void setIntensityLight(float intensity);
    void setIntensityMainScreen(float intensity);
    void setIntensityTopRing(float intensity);
    void setIntensityBottomRing(float intensity);
    void setIntensityMantinel(float intensity);
    void setIntensityBigRing(float intensity);
    void setIntensityMapping(float intensity);

signals:

    void cameraPositionChanged();

private:

    void renderScreens();
    void renderLights();
    void renderMantinelGlass();
    void renderIceField();
    void renderStadium();

    void initializeShaders();
    void updateCameraMatrices();
    void updateLightMatrices(SceneLights::SceneLight light, bool perspective);
    void createDepthBuffer(GLuint &frameBufferID, GLuint &depthTextureID, int textureWidth, int textureHeight, DepthTextureType textureType);
    bool isCollision(glm::vec3 position);

    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent* event);

};

#endif // GL3DSCENEWIDGET_H
