#include "previewsettings.h"
#include "ui_previewsettings.h"

PreviewSettings::PreviewSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PreviewSettings)
{
    ui->setupUi(this);

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(saveSettings()));
    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    ui->info_label->hide();

    loadSettings();
    init();
}

PreviewSettings::~PreviewSettings()
{
    delete ui;
}

void PreviewSettings::init()
{
    emit contentMainScreenChanged(m_sourcePathMainScreen);
    emit contentTopRingChanged(m_sourcePathTopRing);
    emit contentBottomRingChanged(m_sourcePathBottomRing);
    emit contentMantinelChanged(m_sourcePathMantinel);
    emit contentBigRingChanged(m_sourcePathBigRing);
    emit contentMappingChanged(m_sourcePathMapping);

    emit intensityLightChanged(m_intensityLight);

    if(m_playMainScreen){
        emit playMainScreen();
        emit intensityMainScreenChanged(m_intensityMainScreen);
    } else {
        emit intensityMainScreenChanged(0);
    }
    if(m_playTopRing){
        emit playTopRing();
        emit intensityTopRingChanged(m_intensityTopRing);
    } else {
        emit intensityTopRingChanged(0);
    }
    if(m_playBottomRing){
        emit playBottomRing();
        emit intensityBottomRingChanged(m_intensityBottomRing);
    } else {
        emit intensityBottomRingChanged(0);
    }
    if(m_playMantinel){
        emit playMantinel();
        emit intensityMantinelChanged(m_intensityMantinel);
    } else {
        emit intensityMantinelChanged(0);
    }
    if(m_playBigRing){
        emit playBigRing();
        emit intensityBigRingChanged(m_intensityBigRing);
    } else {
        emit intensityBigRingChanged(0);
    }
    if(m_playMapping){
        emit playMapping();
        emit intensityMappingChanged(m_intensityMapping);
    } else {
        emit intensityMappingChanged(0);
    }
}


QString PreviewSettings::pathToContentMainScreen(){
    return m_sourcePathMainScreen;
}

QString PreviewSettings::pathToContentTopRing(){
    return m_sourcePathTopRing;
}

QString PreviewSettings::pathToContentBottomRing(){
    return m_sourcePathBottomRing;
}

QString PreviewSettings::pathToContentBigRing(){
    return m_sourcePathBigRing;
}

QString PreviewSettings::pathToContentMantinel(){
    return m_sourcePathMantinel;
}

float PreviewSettings::intensityLigth(){
    return m_intensityLight;
}

float PreviewSettings::intensityMainScreen(){
    return m_intensityMainScreen;
}

float PreviewSettings::intensityTopRing(){
    return m_intensityTopRing;
}

float PreviewSettings::intensityBottomRing(){
    return m_intensityBottomRing;
}

float PreviewSettings::intensityMantinel(){
    return m_intensityMantinel;
}

float PreviewSettings::intensityBigRing(){
    return m_intensityBigRing;
}

SceneLights::LevelOfDetails PreviewSettings::levelOfDetails(){
    return m_lod;
}

QString PreviewSettings::pathTo3DObjects(){
    return m_pathTo3DObjects;
}

QString PreviewSettings::pathToLightConfig(){
    return m_pathToLightConfig;
}


void PreviewSettings::saveSettings()
{
    QSettings settings;
    settings.setValue("sourcePath_mainScreen", m_sourcePathMainScreen);
    settings.setValue("sourcePath_topRing", m_sourcePathTopRing);
    settings.setValue("sourcePath_bottomRing", m_sourcePathBottomRing);
    settings.setValue("soucePath_mantinel", m_sourcePathMantinel);
    settings.setValue("sourcePath_bigRing", m_sourcePathBigRing);
    settings.setValue("sourcePath_mapping", m_sourcePathMapping);

    settings.setValue("intensity_light", m_intensityLight);
    settings.setValue("intensity_mainScreen", m_intensityMainScreen);
    settings.setValue("intensity_topRing", m_intensityTopRing);
    settings.setValue("intensity_bottomRing", m_intensityBottomRing);
    settings.setValue("intensity_mantinel", m_intensityMantinel);
    settings.setValue("intensity_bigRing", m_intensityBigRing);
    settings.setValue("intensity_mapping", m_intensityMapping);

    settings.setValue("play_mainScreen", m_playMainScreen);
    settings.setValue("play_topRing", m_playTopRing);
    settings.setValue("play_bottomRing", m_playBottomRing);
    settings.setValue("play_mantinel", m_playMantinel);
    settings.setValue("play_bigRing", m_playBigRing);
    settings.setValue("play_mapping", m_playMapping);

    settings.setValue("levelOfDetails", m_lod);
    settings.setValue("pathTo3DObjects", m_pathTo3DObjects);
    settings.setValue("pathToLights", m_pathToLightConfig);
}

void PreviewSettings::loadSettings()
{
    QSettings settings;
    m_sourcePathMainScreen = settings.value("sourcePath_mainScreen", QString()).toString();
    ui->contentMainScreen_lineEdit->setText(m_sourcePathMainScreen);
    m_sourcePathTopRing = settings.value("sourcePath_topRing", QString()).toString();
    ui->contentTopRing_lineEdit->setText(m_sourcePathTopRing);
    m_sourcePathBottomRing = settings.value("sourcePath_bottomRing", QString()).toString();
    ui->contentBottomRing_lineEdit->setText(m_sourcePathBottomRing);
    m_sourcePathMantinel = settings.value("soucePath_mantinel", QString()).toString();
    ui->contentMantinel_lineEdit->setText(m_sourcePathMantinel);
    m_sourcePathBigRing = settings.value("sourcePath_bigRing", QString()).toString();
    ui->contentBigRing_lineEdit->setText(m_sourcePathBigRing);
    m_sourcePathMapping = settings.value("sourcePath_mapping", QString()).toString();
    ui->contentMapping_lineEdit->setText(m_sourcePathMapping);

    m_intensityLight = settings.value("intensity_light", 1).toFloat();
    ui->light_spinBox->setValue(m_intensityLight*mainLightsCoef);
    m_intensityMainScreen = settings.value("intensity_mainScreen", 1).toFloat();
    ui->mainScreen_spinBox->setValue(m_intensityMainScreen*screenLightsCoef);
    m_intensityTopRing = settings.value("intensity_topRing", 1).toFloat();
    ui->topRing_spinBox->setValue(m_intensityTopRing*screenLightsCoef);
    m_intensityBottomRing = settings.value("intensity_bottomRing", 1).toFloat();
    ui->bottomRing_spinBox->setValue(m_intensityBottomRing*screenLightsCoef);
    m_intensityMantinel = settings.value("intensity_mantinel", 1).toFloat();
    ui->mantinel_spinBox->setValue(m_intensityMantinel*screenLightsCoef);
    m_intensityBigRing = settings.value("intensity_bigRing", 1).toFloat();
    ui->bigRing_spinBox->setValue(m_intensityBigRing*screenLightsCoef);
    m_intensityMapping = settings.value("intensity_mapping", 1).toFloat();

    m_playMainScreen = settings.value("play_mainScreen", false).toBool();
    m_playTopRing = settings.value("play_topRing", false).toBool();
    m_playBottomRing = settings.value("play_bottomRing", false).toBool();
    m_playMantinel = settings.value("play_mantinel", false).toBool();
    m_playBigRing = settings.value("play_bigRing", false).toBool();
    m_playMapping = settings.value("play_mapping", false).toBool();

    int lod = settings.value("levelOfDetails", 0).toInt();
    m_lod = static_cast<SceneLights::LevelOfDetails>(lod);
    ui->lod_comboBox->setCurrentIndex(lod);

    m_pathTo3DObjects = settings.value("pathTo3DObjects").toString();
    ui->pathToObj_lineEdit->setText(m_pathTo3DObjects);
    m_pathToLightConfig = settings.value("pathToLights").toString();
    ui->pathToLights_lineEdit->setText(m_pathToLightConfig);
}


void PreviewSettings::on_contentMainScreen_button_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Choose content to play"));

    if(!path.isEmpty()){
        m_sourcePathMainScreen = path;
        ui->contentMainScreen_lineEdit->setText(path);
        emit contentMainScreenChanged(m_sourcePathMainScreen);
    }
}

void PreviewSettings::on_contentTopRing_button_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Choose content to play"));

    if(!path.isEmpty()){
        m_sourcePathTopRing = path;
        ui->contentTopRing_lineEdit->setText(path);
        emit contentTopRingChanged(m_sourcePathTopRing);
    }
}

void PreviewSettings::on_contentBottomRing_button_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Choose content to play"));

    if(!path.isEmpty()){
        m_sourcePathBottomRing = path;
        ui->contentBottomRing_lineEdit->setText(path);
        emit contentBottomRingChanged(m_sourcePathBottomRing);
    }
}

void PreviewSettings::on_contentMantinel_button_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Choose content to play"));

    if(!path.isEmpty()){
        m_sourcePathMantinel = path;
        ui->contentMantinel_lineEdit->setText(path);
        emit contentMantinelChanged(m_sourcePathMantinel);
    }
}

void PreviewSettings::on_contentBigRing_button_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Choose content to play"));

    if(!path.isEmpty()){
        m_sourcePathBigRing = path;
        ui->contentBigRing_lineEdit->setText(path);
        emit contentBigRingChanged(m_sourcePathBigRing);
    }
}

void PreviewSettings::on_contentMapping_button_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Choose content to play"));

    if(!path.isEmpty()){
        m_sourcePathMapping = path;
        ui->contentMapping_lineEdit->setText(path);
        emit contentMappingChanged(m_sourcePathMapping);
    }
}


void PreviewSettings::on_light_spinBox_valueChanged(int arg1)
{
    m_intensityLight = arg1/mainLightsCoef;
    emit intensityLightChanged(m_intensityLight);
}

void PreviewSettings::on_mainScreen_spinBox_valueChanged(int arg1)
{
    m_intensityMainScreen = arg1/screenLightsCoef;
    if(m_playMainScreen){
        emit intensityMainScreenChanged(m_intensityMainScreen);
    }
}

void PreviewSettings::on_topRing_spinBox_valueChanged(int arg1)
{
    m_intensityTopRing = arg1/screenLightsCoef;
    if(m_playTopRing){
        emit intensityTopRingChanged(m_intensityTopRing);
    }
}

void PreviewSettings::on_bottomRing_spinBox_valueChanged(int arg1)
{
    m_intensityBottomRing = arg1/screenLightsCoef;
    if(m_playBottomRing){
        emit intensityBottomRingChanged(m_intensityBottomRing);
    }
}

void PreviewSettings::on_mantinel_spinBox_valueChanged(int arg1)
{
    m_intensityMantinel = arg1/screenLightsCoef;
    if(m_playMantinel){
        emit intensityMantinelChanged(m_intensityMantinel);
    }
}

void PreviewSettings::on_bigRing_spinBox_valueChanged(int arg1)
{
    m_intensityBigRing = arg1/screenLightsCoef;
    if(m_playBigRing){
        emit intensityBigRingChanged(m_intensityBigRing);
    }
}

void PreviewSettings::on_mapping_spinBox_valueChanged(int arg1)
{
    m_intensityMapping = arg1/screenLightsCoef;
    if(m_playMapping){
        emit intensityMappingChanged(m_intensityMapping);
    }
}


void PreviewSettings::on_playMainScreen_button_clicked(){
    emit playMainScreen();
    emit intensityMainScreenChanged(m_intensityMainScreen);
    m_playMainScreen = true;
}

void PreviewSettings::on_playTopRing_button_clicked(){
    emit playTopRing();
    emit intensityTopRingChanged(m_intensityTopRing);
    m_playTopRing = true;
}

void PreviewSettings::on_playBottomRing_button_clicked(){
    emit playBottomRing();
    emit intensityBottomRingChanged(m_intensityBottomRing);
    m_playBottomRing = true;
}

void PreviewSettings::on_playMantinel_button_clicked(){
    emit playMantinel();
    emit intensityMantinelChanged(m_intensityMantinel);
    m_playMantinel = true;
}

void PreviewSettings::on_playBigRing_button_clicked(){
    emit playBigRing();
    emit intensityBigRingChanged(m_intensityBigRing);
    m_playBigRing = true;
}

void PreviewSettings::on_playMapping_button_clicked(){
    emit playMapping();
    emit intensityMappingChanged(m_intensityMapping);
    m_playMapping = true;
}

void PreviewSettings::on_stopMainScreen_button_clicked(){
    emit stopMainScreen();
    emit intensityMainScreenChanged(0);
    m_playMainScreen = false;
}

void PreviewSettings::on_stopTopRing_button_clicked(){
    emit stopTopRing();
    emit intensityTopRingChanged(0);
    m_playTopRing = false;
}

void PreviewSettings::on_stopBottomRing_button_clicked(){
    emit stopBottomRing();
    emit intensityBottomRingChanged(0);
    m_playBottomRing = false;
}

void PreviewSettings::on_stopMantinel_button_clicked(){
    emit stopMantinel();
    emit intensityMantinelChanged(0);
    m_playMantinel = false;
}

void PreviewSettings::on_stopBigRing_button_clicked(){
    emit stopBigRing();
    emit intensityBigRingChanged(0);
    m_playBigRing = false;
}

void PreviewSettings::on_stopMapping_button_clicked(){
    emit stopMapping();
    emit intensityMappingChanged(0);
    m_playMapping = false;
}


void PreviewSettings::on_lod_comboBox_activated(int index)
{
    SceneLights::LevelOfDetails lod = static_cast<SceneLights::LevelOfDetails>(index);
    if(m_lod != lod){
        ui->info_label->show();
        m_lod = lod;
    }
}

void PreviewSettings::on_pathToObj_pushButton_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, tr("Choose folder with 3D objects"));

    if(!path.isEmpty()){
        m_pathTo3DObjects = path;
        ui->pathToObj_lineEdit->setText(path);
        ui->info_label->show();
    }
}

void PreviewSettings::on_pathToLights_pushButton_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("Choose lights config file"));

    if(!path.isEmpty()){
        m_pathToLightConfig = path;
        ui->pathToLights_lineEdit->setText(path);
        ui->info_label->show();
    }
}

