#include "shaderloader.h"

using namespace std ; 

// This is a basic program to initiate a shader
// The textFileRead function reads in a filename into a string
// programerrors and shadererrors output compilation errors
// initshaders initiates a vertex or fragment shader
// initprogram initiates a program with vertex and fragment shaders

string ShaderLoader::textFileRead (const char * pathToFile)
{
    QDir currentDir(QCoreApplication::applicationDirPath());
    QFile file(currentDir.absoluteFilePath(pathToFile));
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "[ERROR]\t ObjectLoader::loadObject - unable to open file: " << file.fileName();
        return QString().toStdString();
    }

    QTextStream stream(&file);

    QString shaderText;
    while (!stream.atEnd()) {
        shaderText += stream.readLine() + "\n";
    }

    file.close();
    return shaderText.toStdString();
}

void ShaderLoader::programerrors (const GLint program)
{
  GLint length ; 
  GLchar * log ; 
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length) ; 
  log = new GLchar[length+1] ;
  glGetProgramInfoLog(program, length, &length, log) ; 
  cout << "Compile Error, Log Below\n" << log << "\n" ; 
  delete [] log ; 
}

void ShaderLoader::shadererrors (const GLint shader) {
  GLint length ; 
  GLchar * log ; 
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length) ; 
  log = new GLchar[length+1] ;
  glGetShaderInfoLog(shader, length, &length, log) ; 
  cout << "Compile Error, Log Below\n" << log << "\n" ; 
  delete [] log ; 
}

GLuint ShaderLoader::initshaders (GLenum type, const char *filename)
{
  // Using GLSL shaders, OpenGL book, page 679 

  GLuint shader = glCreateShader(type) ; 
  GLint compiled ; 
  string str = textFileRead (filename) ; 
//  GLchar * cstr = new GLchar[str.size()+1] ;
  GLchar cstr[str.size()+1];
  const GLchar * cstr2 = cstr ;
  strcpy(cstr,str.c_str()) ; 
  glShaderSource (shader, 1, &cstr2, NULL) ;
  glCompileShader (shader) ; 
  glGetShaderiv (shader, GL_COMPILE_STATUS, &compiled) ; 
  if (!compiled) { 
    shadererrors (shader) ; 
    throw 3;
  }
  return shader ; 
}

GLuint ShaderLoader::initprogram (GLuint vertexshader, GLuint fragmentshader)
{
  GLuint program = glCreateProgram() ; 
  GLint linked ; 
  glAttachShader(program, vertexshader) ; 
  glAttachShader(program, fragmentshader) ; 
  glLinkProgram(program) ; 
  glGetProgramiv(program, GL_LINK_STATUS, &linked) ; 
  if (linked) glUseProgram(program) ; 
  else { 
    programerrors(program) ; 
    throw 4 ; 
  }
  return program ; 
}
