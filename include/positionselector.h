#ifndef POSITIONSELECTOR_H
#define POSITIONSELECTOR_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QMouseEvent>
#include <QVector>
#include <QPoint>
#include <QPainter>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>


class PositionSelector : public QLabel
{
    Q_OBJECT

    const float clickDistanceTolerance = 5.0f;
    const int mapWidth = 600; // must have same size as hight map in gl3dscenewidget
    const int mapHeight = 400; // must have same size as hight map in gl3dscenewidget

    QVector<QPointF> m_selectedPositions;
    QPointF m_currentPosition;
    QImage m_image;

public:

    PositionSelector(QString pathToPicture, QWidget* parent = 0);
    ~PositionSelector();

    void savePoints();
    void loadPoints();

    void switchToNextCamera();

public slots:

    void cameraPositionChanged();

signals:

    void positionChanged(QPointF position);

private:

    void removePoint(QPointF position);
    void selectPoint(QPointF position);

    bool pointsAreClose(QPointF point1, QPointF point2);
    bool positionOutOfStadium(QPointF position);

//    QPointF getRelativePoint(QPointF point); // map coordinates of point from widget coordinates to range [-1,1]
//    QPointF getLocalPoint(QPointF point); // map coordinates of point from range [-1,1] to widget coordinates

    void mousePressEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent *);
};

#endif // POSITIONSELECTOR_H
