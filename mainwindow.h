#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "include/gl3dscenewidget.h"
#include "include/previewsettings.h"
#include "include/mediaplayer.h"
#include "include/positionselector.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    Ui::MainWindow *ui;

    MediaPlayer* playerCube = nullptr;
    MediaPlayer* playerTopRing = nullptr;
    MediaPlayer* playerBottomRing = nullptr;
    MediaPlayer* playerMantinel = nullptr;
    MediaPlayer* playerBigRing = nullptr;
    MediaPlayer* playerMapping = nullptr;

    PreviewSettings* previewSettings = nullptr;

    PositionSelector* positionSelector = nullptr;

    GL3DSceneWidget* sceneWidget = nullptr;

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_menu_button_clicked();
    void on_map_button_clicked();

private:

    bool eventFilter(QObject *object, QEvent *event);
};

#endif // MAINWINDOW_H
