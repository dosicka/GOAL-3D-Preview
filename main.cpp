#include "mainwindow.h"
#include <QApplication>
#include <QGLFormat>
#include <QSurfaceFormat>

int main(int argc, char *argv[])
{

    // zde bych měl nastavit Qt format (resp. context) pro OpenGL.
    // Mělo by jit nastavit i depth testing a depth buffer na 32 bit,
    // aby nebyl problém se z-fightingem (popsáno v texťáku k DP)
    // ovšem nastavení mě nefungoval

//    QSurfaceFormat format;
//    format.setRenderableType(QSurfaceFormat::OpenGL);
//    format.setDepthBufferSize( 24 );
//    format.setMajorVersion( 4 );
//    format.setMinorVersion( 3 );
//    format.setSamples( 4 );
//    format.setProfile(QSurfaceFormat::CompatibilityProfile);
//    QSurfaceFormat::setDefaultFormat(format);

//    QGLFormat format; // = QGLFormat::defaultFormat();
//    format.setDepth(true);
//    format.setDepthBufferSize(24);
//    QGLFormat::setDefaultFormat(format);

    // set global variable: Qt scale to hdpi displays
    qputenv("QT_SCREEN_SCALE_FACTORS", "1"); // 1 = enable hdpi scaling, 0 = disable hdpi scaling

    QApplication a(argc, argv);

    MainWindow w;
    w.show();

    return a.exec();
}
