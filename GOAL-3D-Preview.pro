#-------------------------------------------------
#
# Project created by QtCreator 2017-07-22T12:15:09
#
#-------------------------------------------------

QT += core gui widgets opengl

TARGET = GOAL-3D-Preview
TEMPLATE = app

CONFIG += c++11

# for VLC player
LIBS +=-lvlc -lvlccore
INCLUDEPATH += /usr/include/vlc/plugins /usr/local/cuda/include /opt/cuda/include/

SOURCES += main.cpp\
    mainwindow.cpp \
    include/gl3dscenewidget.cpp \
    include/previewsettings.cpp \
    include/entities/scenecamera.cpp \
    include/shaderloader.cpp \
    include/entities/object3d.cpp \
    include/entities/texture.cpp \
    include/entities/scenelights.cpp \
    include/positionselector.cpp \
    include/mediaplayer.cpp

HEADERS  += mainwindow.h \
    include/gl3dscenewidget.h \
    include/previewsettings.h \
    include/entities/scenecamera.h \
    include/shaderloader.h \
    include/entities/object3d.h \
    include/entities/texture.h \
    include/entities/scenelights.h \
    include/positionselector.h \
    include/mediaplayer.h

FORMS    += mainwindow.ui \
    include/previewsettings.ui

RESOURCES += \
    resources.qrc
