#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->installEventFilter(this);

    QCoreApplication::setOrganizationName("Daite");
    QCoreApplication::setOrganizationDomain("daite.cz");
    QCoreApplication::setApplicationName("GOAL-3D-Preview");
    QCoreApplication::setApplicationVersion("1.0");

    // PREVIEW SETTINGS
    previewSettings = new PreviewSettings(this);

    // 3D SCENE WIDGET
    sceneWidget = new GL3DSceneWidget(this);
    sceneWidget->installEventFilter(this);
    ui->scene3D_layout->addWidget(sceneWidget);
    sceneWidget->setPathTo3DObjects(previewSettings->pathTo3DObjects());
    sceneWidget->setPathToLightConfig(previewSettings->pathToLightConfig());
    sceneWidget->setLevelOfDetails(previewSettings->levelOfDetails());

    connect(previewSettings, SIGNAL(intensityLightChanged(float)), sceneWidget, SLOT(setIntensityLight(float)));
    connect(previewSettings, SIGNAL(intensityMainScreenChanged(float)), sceneWidget, SLOT(setIntensityMainScreen(float)));
    connect(previewSettings, SIGNAL(intensityTopRingChanged(float)), sceneWidget, SLOT(setIntensityTopRing(float)));
    connect(previewSettings, SIGNAL(intensityBottomRingChanged(float)), sceneWidget, SLOT(setIntensityBottomRing(float)));
    connect(previewSettings, SIGNAL(intensityMantinelChanged(float)), sceneWidget, SLOT(setIntensityMantinel(float)));
    connect(previewSettings, SIGNAL(intensityBigRingChanged(float)), sceneWidget, SLOT(setIntensityBigRing(float)));
    connect(previewSettings, SIGNAL(intensityMappingChanged(float)), sceneWidget, SLOT(setIntensityMapping(float)));

    // VIDEO PLAYERS
    playerCube = new MediaPlayer();
    playerTopRing = new MediaPlayer();
    playerBottomRing = new MediaPlayer();
    playerMantinel = new MediaPlayer();
    playerBigRing = new MediaPlayer();
    playerMapping = new MediaPlayer();

    connect(playerCube, SIGNAL(imageReady(QImage,int)), sceneWidget, SLOT(setFrameToMainScreen(QImage,int)), Qt::QueuedConnection);
    connect(playerTopRing, SIGNAL(imageReady(QImage,int)), sceneWidget, SLOT(setFrameToTopRing(QImage,int)), Qt::QueuedConnection);
    connect(playerBottomRing, SIGNAL(imageReady(QImage,int)), sceneWidget, SLOT(setFrameToBottomRing(QImage,int)), Qt::QueuedConnection);
    connect(playerMantinel, SIGNAL(imageReady(QImage,int)), sceneWidget, SLOT(setFrameToMantinel(QImage,int)), Qt::QueuedConnection);
    connect(playerBigRing, SIGNAL(imageReady(QImage,int)), sceneWidget, SLOT(setFrameToBigRing(QImage,int)), Qt::QueuedConnection);
    connect(playerMapping, SIGNAL(imageReady(QImage,int)), sceneWidget, SLOT(setFrameToMapping(QImage,int)), Qt::QueuedConnection);

    connect(previewSettings, SIGNAL(contentMainScreenChanged(QString)), playerCube, SLOT(setMedia(QString)));
    connect(previewSettings, SIGNAL(contentTopRingChanged(QString)), playerTopRing, SLOT(setMedia(QString)));
    connect(previewSettings, SIGNAL(contentBottomRingChanged(QString)), playerBottomRing, SLOT(setMedia(QString)));
    connect(previewSettings, SIGNAL(contentMantinelChanged(QString)), playerMantinel, SLOT(setMedia(QString)));
    connect(previewSettings, SIGNAL(contentBigRingChanged(QString)), playerBigRing, SLOT(setMedia(QString)));
    connect(previewSettings, SIGNAL(contentMappingChanged(QString)), playerMapping, SLOT(setMedia(QString)));

    connect(previewSettings, SIGNAL(playMainScreen()), playerCube, SLOT(play()));
    connect(previewSettings, SIGNAL(playTopRing()), playerTopRing, SLOT(play()));
    connect(previewSettings, SIGNAL(playBottomRing()), playerBottomRing, SLOT(play()));
    connect(previewSettings, SIGNAL(playMantinel()), playerMantinel, SLOT(play()));
    connect(previewSettings, SIGNAL(playBigRing()), playerBigRing, SLOT(play()));
    connect(previewSettings, SIGNAL(playMapping()), playerMapping, SLOT(play()));

    connect(previewSettings, SIGNAL(stopMainScreen()), playerCube, SLOT(stop()));
    connect(previewSettings, SIGNAL(stopTopRing()), playerTopRing, SLOT(stop()));
    connect(previewSettings, SIGNAL(stopBottomRing()), playerBottomRing, SLOT(stop()));
    connect(previewSettings, SIGNAL(stopMantinel()), playerMantinel, SLOT(stop()));
    connect(previewSettings, SIGNAL(stopBigRing()), playerBigRing, SLOT(stop()));
    connect(previewSettings, SIGNAL(stopMapping()), playerMapping, SLOT(stop()));


    previewSettings->init();

    // POSITION SELECTOR
    positionSelector = new PositionSelector(":/textures/resources/stadium_scheme.png", this);
    positionSelector->installEventFilter(this);
    positionSelector->setWindowFlags(Qt::Dialog);
    positionSelector->setWindowTitle("Scheme");
    positionSelector->hide();
    connect(positionSelector, SIGNAL(positionChanged(QPointF)), sceneWidget, SLOT(setCameraPosition(QPointF)));
    connect(sceneWidget, SIGNAL(cameraPositionChanged()), positionSelector, SLOT(cameraPositionChanged()));
    positionSelector->loadPoints();
}

MainWindow::~MainWindow()
{
    delete ui;
    if(playerCube != nullptr){
        playerCube->stop();
        delete playerCube;
    }
    if(playerTopRing != nullptr){
        playerTopRing->stop();
        delete playerTopRing;
    }
    if(playerBottomRing != nullptr){
        playerBottomRing->stop();
        delete playerBottomRing;
    }
    if(playerMantinel != nullptr){
        playerMantinel->stop();
        delete playerMantinel;
    }
    if(playerBigRing != nullptr){
        playerBigRing->stop();
        delete playerBigRing;
    }
    if(playerMapping != nullptr){
        playerMapping->stop();
        delete playerMapping;
    }
}


void MainWindow::on_menu_button_clicked()
{
    previewSettings->show();
}

void MainWindow::on_map_button_clicked()
{
    positionSelector->show();
}


bool MainWindow::eventFilter(QObject *, QEvent *event)
{
    if (event->type() == QEvent::KeyPress) {
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);

        if(keyEvent->key() == Qt::Key_Up){
            sceneWidget->zoomCamera(10);
            return true;
        } else if(keyEvent->key() == Qt::Key_Down){
            sceneWidget->zoomCamera(-10);
            return true;
        } else if(keyEvent->key() == Qt::Key_Left){
            sceneWidget->translateCamera(0.01, 0);
            return true;
        } else if(keyEvent->key() == Qt::Key_Right){
            sceneWidget->translateCamera(-0.01, 0);
            return true;
        }
        else if(keyEvent->key() == Qt::Key_C){
            positionSelector->switchToNextCamera();
            return true;
        }
    }
    return false;
}
